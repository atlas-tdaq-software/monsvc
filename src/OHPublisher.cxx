/*
 * OHPublisher.cpp
 *
 *  Created on: Oct 19, 2012
 *      Author: ctalau
 */

#include "OHPublisher.h"
#include "monsvc/Exceptions.h"
#include "monsvc/Tag.h"

#include <oh/exceptions.h>

#include <TObject.h>
#include <TGraph.h>
#include <TGraph2D.h>
#include <TEfficiency.h>
#include <TH1.h>

using std::string;
using std::make_shared;
using std::shared_ptr;

namespace monsvc {

    namespace {
        shared_ptr<OHRootProvider> create_root_provider(const IPCPartition& partition,
                                                        const string& server_name, const string& provider_name,
                                                        shared_ptr<OHCommandListener> cmdlistener)
        {
            try {
              return make_shared<OHRootProvider>(partition, server_name, 
                                                 provider_name, 
                                                 cmdlistener.get());
            } catch (daq::oh::Exception& oh_exception) {
                throw CannotCreatePublisher(ERS_HERE, "OH", oh_exception);
            }
            return shared_ptr<OHRootProvider>(); // to make the eclipse compiler happy.
        }
    }

    OHPublisher::OHPublisher(const IPCPartition& partition, 
                             const string& server_name,
                             const string& provider_name,
                             shared_ptr<OHCommandListener> cmdlistener) :
      m_provider(create_root_provider(partition, server_name, provider_name, cmdlistener)), m_cmdlistener(cmdlistener)
    {
    }

    OHPublisher::~OHPublisher()
    {
    }


    template <typename T>
    void OHPublisher::do_publish(const string& name, const Tag& tag,
                                 const Annotations& ann, T * obj)
    {
        try {
            if(tag)
                m_provider->publish(*obj, name, tag.value, ann);
            else
                m_provider->publish(*obj, name, -1, ann);
        } catch (daq::oh::Exception& oh_exception) {
            OHPublishingIssue issue(ERS_HERE, name, oh_exception);
            throw issue;
        }
    }

    void OHPublisher::publish(const string& name, const Tag& tag,
                              const Annotations& ann, TObject *obj)
    {
       auto th1 = dynamic_cast<TH1 *>(obj);
       if (th1) {
           do_publish(name, tag, ann, th1);
           return;
       }
       
       auto tg = dynamic_cast<TGraph *>(obj);
       if (tg) {
           do_publish(name, tag, ann, tg);
           return;
       }
       
       auto tg2d = dynamic_cast<TGraph2D *>(obj);
       if (tg2d) {
           do_publish(name, tag, ann, tg2d);
           return;
       }

       auto teff = dynamic_cast<TEfficiency *>(obj);
       if (teff) {
           do_publish(name, tag, ann, teff);
           return;
       }

       OHPublishingInvalidType issue(ERS_HERE, name);
       ers::warning(issue);
    }

    void OHPublisher::publish(const string& name, const Tag& tag,
                              const Annotations& ann, TH1 *obj)
    {
        do_publish(name, tag, ann, obj);
    }

    void OHPublisher::publish(const string& name, const Tag& tag,
                              const Annotations& ann, TGraph *obj)
    {
        do_publish(name, tag, ann, obj);
    }

    void OHPublisher::publish(const string& name, const Tag& tag,
                              const Annotations& ann, TGraph2D *obj)
    {
        do_publish(name, tag, ann, obj);
    }

    void OHPublisher::publish(const string& name, const Tag& tag,
                              const Annotations& ann, TEfficiency *obj)
    {
        do_publish(name, tag, ann, obj);
    }
} /* namespace monsvc */

/* Local Variables: */
/* c-basic-offset:4 */
/* End: */
