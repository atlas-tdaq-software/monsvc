// this is -*- c++ -*-
#ifndef MONSVC_FILEPUBLISHER_H_
#define MONSVC_FILEPUBLISHER_H_

#include "monsvc/PublisherBase.h"

class TFile;

namespace monsvc {

    /**
     * @brief Publisher to a ROOT file.
     *
     * An implementation of a publisher that writes
     * ROOT based objects to a TFile and ignores
     * ISInfo objects.
     */
    class FilePublisher : public PublisherBase {
    public:
        /** @brief Constructs a publisher that writes objects to a ROOT file. */
        FilePublisher(TFile * root_file);
        ~FilePublisher();

        // visitor interface
        using PublisherBase::publish;
        using PublisherBase::knows;

        /** @brief Writes the TObject object to a ROOT file. */
        virtual void
        publish(const std::string&, const Tag&,
                const Annotations&, TObject *) override;
        virtual bool knows(TObject*) override { return true; }
        
        /** @brief Writes the TH1 object to a ROOT file. */
        virtual void
        publish(const std::string&, const Tag&,
                const Annotations&, TH1 *) override;
        virtual bool knows(TH1*) override { return true; }

        /** @brief Writes the TGraph object to a ROOT file. */
        virtual void
        publish(const std::string&, const Tag&,
                const Annotations&, TGraph *) override;
        virtual bool knows(TGraph*) override { return true; }

        /** @brief Writes the TGraph2D object to a ROOT file. */
        virtual void
        publish(const std::string&, const Tag&,
                const Annotations&, TGraph2D *) override;
        virtual bool knows(TGraph2D*) override { return true; }

        /** @brief Writes the TEfficiency object to a ROOT file. */
        virtual void
        publish(const std::string&, const Tag&,
                const Annotations&, TEfficiency *) override;
        virtual bool knows(TEfficiency*) override { return true; }

    private:
        TFile                   *m_root_file;    ///< The ROOT file.
    };
}

#endif // MONSVC_FILEPUBLISHER_H_

/* Local Variables: */
/* c-basic-offset:4 */
/* End: */
