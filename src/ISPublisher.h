// this is -*- c++ -*-
#ifndef MONSVC_ISPUBLISHER_H_
#define MONSVC_ISPUBLISHER_H_

#include "monsvc/PublisherBase.h"

#include <memory>

class IPCPartition;
class ISInfoDictionary;

namespace monsvc {

    /**
     * @brief Implementation of publisher that sends the information to an IS server.
     *
     * It publishes objects of type ISInfo or TObject. The latter version works
     * for simple enough objects (i.e. whose members have only basic data types,
     * arrays and strings). These objects are converted to ISInfo, so one does
     * not need the original TObject in order to read them. The TObject must
     * have a dictionary in order to be published.
     */
    class ISPublisher : public PublisherBase {
    public:
        /**
         * @brief Constructs an IS publisher.
         *
         * @param partition the IPC partition in which the current application is running.
         * @param app_name the name of the current application.
         * @param server_name the name of the IS server used for publishing.
         */
        explicit ISPublisher(const IPCPartition& partition, const std::string& server_name,
                const std::string& app_name);
        ~ISPublisher();

        // visitor interface from PublisherBase
        using PublisherBase::publish;
        using PublisherBase::knows;
        
        /**
         * @brief Represents the TObject as ISInfo and publishes it in the IS server.
         */
        virtual void publish(const std::string&, const Tag&, 
                             const Annotations&, TObject *) override;
        virtual bool knows(TObject *) override { return true; }

        /**
         * @brief Publishes the ISInfo object in the IS server.
         */
        virtual void publish(const std::string&, const Tag&,
                             const Annotations&, ISInfo *) override;
        virtual bool knows(ISInfo *) override { return true; }

    private:
        const std::string m_server_name;
        const std::string m_app_name;
        std::shared_ptr<ISInfoDictionary> m_dict;
    };
}

#endif // MONSVC_ISPUBLISHER_H_

/* Local Variables: */
/* c-basic-offset:4 */
/* End: */
