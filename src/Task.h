// -*- c++ -*-

#ifndef MONSVC_TASK_H_
#define MONSVC_TASK_H_

#include "monsvc/NameFilter.h"
#include "monsvc/PublisherBase.h"

#include <memory>

namespace monsvc {
  
  namespace detail {
  
     /*
      * @brief Prototype class for a task to be executed by the
      * scheduler
      *
      * At this level it only exposes the minimal interface needed
      * by the PublishingController. Schedulers (and unit testing) 
      * can inherit and expand.
      */
    
    class Task {
    public:
      Task(std::shared_ptr<PublisherBase> publisher,
           const NameFilter& filter):
      m_filter(filter), m_publisher(publisher) {}
      virtual ~Task() {}

      /** @brief Performs a final publication: all matching objects 
       are published, possibly adding specific qualifiers. */
      virtual void finalpublication() = 0;
      
    protected:
      NameFilter m_filter;
      std::shared_ptr<PublisherBase> m_publisher;
    };
    
  }
} /* namespace monsvc */
#endif /* MONSVC_TASK_H_ */
