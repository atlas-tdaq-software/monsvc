// -*- c++ -*-

/*
 * PublishingScheduler.h
 *
 *  Created on: Oct 25, 2012
 *      Author: ctalau
 */

#ifndef MONSVC_PUBLISHINGSCHEDULER_H_
#define MONSVC_PUBLISHINGSCHEDULER_H_

#include "monsvc/PublisherBase.h"
#include "monsvc/NameFilter.h"
#include "Task.h"

#include <boost/date_time/posix_time/posix_time_types.hpp>
#include <boost/noncopyable.hpp>
#include <boost/thread.hpp>

#include <list>
#include <map>
#include <utility>
#include <vector>

namespace monsvc {
    namespace detail {

        /*
         * @brief Class used to schedule generic tasks to run periodically.
         *
         * Generic tasks can be scheduled to be executed periodically with 
         * a given interval. An interval can be optionally broken down in 
         * slots. The tasks are provided with information on the ongoing slot. 
         * Every running task has a handle that can be used to stop the
         * execution of that task.
         *
         * Tasks can be added/removed even after the scheduler started execution.
         * The scheduler can be restarted.
         *
         * The class is not thread safe, so external synchronization is 
         * required when calling its methods.
         */
        class PeriodicScheduler : boost::noncopyable {
        public:
        

            /**
             * @brief Task definition for the PeriodicScheduler
             *
             * @param publisher Instance of a publisher used by this task
             * @param filter Only names passing this filter will be published by this task.
             * @param preceedingFilters Filters of the tasks that preceed this task, if a name
             *                          passes any of these filters it is ignored by this task.
             */
            class PeriodicTask : public Task {
            public:
                PeriodicTask(std::shared_ptr<PublisherBase> publisher,
                             const NameFilter& filter,
                             const std::vector<NameFilter>& preceedingFilters=std::vector<NameFilter>()):
                    Task(publisher, filter), m_preceedingFilters(preceedingFilters) {}
                virtual ~PeriodicTask() {}
                
                virtual void finalpublication() override;
                /** @brief Publishes object using the provided 
                 *  slot information.
                */ 
                virtual void publish(unsigned int, unsigned int);

            private:
                std::vector<NameFilter> m_preceedingFilters;
            };
          
            /**
             * @brief The indexing type of worker threads.
             */
            typedef std::pair<unsigned int, unsigned int> WorkerType;
          
            /**
             * @brief Worker thread pointer to tasks.
             */
            typedef std::shared_ptr<PeriodicTask> PeriodicTaskPtr;

            /**
             * @brief A handle for a scheduled task.
             *
             * After the task is removed from the scheduler, the handle becomes invalid.
             */
            typedef std::pair<WorkerType, std::list<PeriodicTaskPtr>::iterator> 
            PeriodicTaskHandle;

            /*
             * @brief Strategy class used to sleep interruptibly.
             *
             * When the scheduler is idle, it uses this class to sleep.
             */
            class TimeManager {
            public:
                /** @brief Makes the current thread sleep. */
                virtual void sleep(boost::posix_time::time_duration) = 0;
                /** @brief Queries the current time */
                virtual boost::posix_time::ptime now() = 0;

                virtual ~TimeManager() {}
            };

            /**
             * @brief Construct a PeriodicScheduler that calls the @c sleeper when idle.
             */
            explicit PeriodicScheduler(TimeManager& time_manager);
            PeriodicScheduler();

            ~PeriodicScheduler();

            /**
             * @brief Schedules a task to run periodically.
             *
             * @param task the task to be executed periodically.
             * @param interval the interval in seconds between two executions.
             * @param the number of slot the interval should be split into
             * @param sync if set, the task will be sync to a absolute 
             * time which is an integer multiple of the interval
             *
             * @return a handle to be passed in for descheduling the task.
             */
            PeriodicTaskHandle schedule_task(PeriodicTaskPtr task,
                                             unsigned int interval,
                                             unsigned int nslots, bool sync);

            /**
             * @brief Removes from the scheduler the task corresponding to the handle.
             *
             * @param handle the handle of the task to be removed from the scheduler.
             */
            void deschedule_task(PeriodicTaskHandle& handle);

            /**
             * @brief Start executing the scheduled tasks.
             */
            void start();

            /**
             * @brief Stops all the scheduled tasks.
             */
            void stop();

        private:
            class WorkerThread;
            typedef std::map<WorkerType, std::shared_ptr<WorkerThread> > ThreadMap;
            ThreadMap m_thread_map;

            bool m_running;
            TimeManager& m_time_manager;
        };
    }

} /* namespace monsvc */
#endif /* MONSVC_PUBLISHINGSCHEDULER_H_ */

/* Local Variables: */
/* c-basic-offset:4 */
/* End: */
