#include "ISPublisher.h"
#include "monsvc/Exceptions.h"
#include "monsvc/Tag.h"

#include "TInfo.h"

#include <ipc/partition.h>
#include <is/infodictionary.h>
#include <is/exceptions.h>

using std::string;
using std::stringstream;

namespace monsvc {

    ISPublisher::ISPublisher(const IPCPartition& partition, const string& server_name,
            const string& app_name) :
            m_server_name(server_name),
            m_app_name(app_name),
            m_dict(new ISInfoDictionary(partition))
    {
    }

    ISPublisher::~ISPublisher()
    {
    }

  void ISPublisher::publish(const string& name, const Tag& tag,
                            const Annotations& ann, TObject *obj)
    {
        detail::TInfo info(obj);
        publish(name, tag, ann, &info);
    }

    void ISPublisher::publish(const string& name, const Tag& tag,
                              const Annotations&, ISInfo *obj)
    {
        stringstream full_name;
        full_name << m_server_name << '.' << m_app_name << '.' << name;
        try {
            if (tag) {
                m_dict->checkin(full_name.str(), tag.value, *obj);
            } else {
                m_dict->checkin(full_name.str(), *obj);
            }
        } catch (const daq::is::Exception& is_exception) {
            ISPublishingIssue issue(ERS_HERE, name, is_exception);
            throw issue;
        }
    }
}

/* Local Variables: */
/* c-basic-offset:4 */
/* End: */
