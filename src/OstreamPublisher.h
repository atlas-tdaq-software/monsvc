// this is -*- c++ -*-
#ifndef MONSVC_OSTREAMPUBLISHER_H_
#define MONSVC_OSTREAMPUBLISHER_H_

#include "monsvc/PublisherBase.h"

#include <string>
#include <iosfwd>

class TFile;

namespace monsvc {

    /**
     * @brief An implementation of a publisher, writing objects to an std::ostream.
     *
     * This class ignores histograms, while TObject objects are written only to std::cout.
     */
    class OstreamPublisher : public PublisherBase {
    public:
        /** @brief Contructs a publisher that writes objects to an output stream. */
        OstreamPublisher(std::ostream& os);
        ~OstreamPublisher();

        // visitor interface
        using PublisherBase::publish;
        using PublisherBase::knows;

        /** @brief Prints the TObject to std::cout. */
        virtual void 
        publish(const std::string&, const Tag&,
                const Annotations&, TObject *) override;
        virtual bool knows(TObject *) override { return true; }

        /** @brief Prints the ISInfo object to the given stream. */
        virtual void 
        publish(const std::string&, const Tag&,
                const Annotations&, ISInfo *) override;
        virtual bool knows(ISInfo *) override { return true; }

     private:
        std::ostream            &m_stream;
    };

}

#endif // MONSVC_OSTREAMPUBLISHER_H_

/* Local Variables: */
/* c-basic-offset:4 */
/* End: */
