
#include "OHCommander.h"
#include "Registry.h"
#include "monsvc/Exceptions.h"

#include <TH1.h>

#include <boost/tokenizer.hpp>
#include <functional>
#include <algorithm>

namespace monsvc {

  namespace detail {


    std::unordered_map<std::string, std::weak_ptr<OHCommander>>&
      commanderstore(){
      static std::unordered_map<std::string, std::weak_ptr<OHCommander>> ohcommanders;
      return ohcommanders;
    }

    std::shared_ptr<OHCommandListener> 
      create_commander(const std::string& server_name, 
                       const std::string& provider_name, 
                       const NameFilter& filter){
      auto key = server_name+provider_name;
      auto commanders = commanderstore();
      auto it = commanders.find(key);
      std::shared_ptr<OHCommander> cmdlst;
      if (it != commanders.end() && not it->second.expired()) {
        cmdlst = it->second.lock();
      } else {
        cmdlst = std::make_shared<OHCommander>();
        commanders[key] = cmdlst;
      }
      
      cmdlst->addfilter(filter);
      return cmdlst;
    }

    namespace {
      enum commandtype : unsigned int {
        unknown = 0,
          reset = 1,
          };
      
      Command parsecommand(const std::string& command){
        boost::char_separator<char> sep(";");
        boost::tokenizer<boost::char_separator<char> > tokens(command, sep);
        
        boost::tokenizer<boost::char_separator<char> >::const_iterator tokenit =
          tokens.begin();
        
        //First element is the command type
        std::string cmdtype("");
        if(tokenit != tokens.end()){
          cmdtype = *tokenit;
        }

        Command cmd;
        //(optional) arguments follow
        cmd.m_args.assign(++tokenit,tokens.end());
        
        if(cmdtype == "reset"){
          cmd.m_cmdtype = commandtype::reset; 
        } else {
          cmd.m_cmdtype = commandtype::unknown; 
        }
        
        return cmd;
      }
      
      void collect(const detail::Registry::Entry obj, 
                   std::vector<detail::Registry::Entry>& objs,
                   const NameFilter& flt){
          if (flt.matches(obj->name())) {
            objs.push_back(obj);
          }
      }

    }

    void OHCommander::addfilter(const NameFilter& filter){
      m_filters.push_back(filter);
    }

    void OHCommander::command(const std::string& histogram_name, 
                              const std::string& command){

      auto cmd = parsecommand(command);
      if(cmd.m_cmdtype == commandtype::unknown){
        UnknownCommand issue(ERS_HERE, command);
        ers::warning(issue);
        return;
      }
      
      try {
        auto histos = Registry::instance().find_objects(histogram_name);
        for(auto& histo: histos) {
          histo->command(*this, cmd);
        }
      } catch (NotFound& ex){
        HistogramNotFound issue(ERS_HERE, histogram_name, ex);
        ers::warning(issue);
      }
    }

    void OHCommander::command(const std::string& command){
      
      auto cmd = parsecommand(command);
      if(cmd.m_cmdtype == commandtype::unknown){
        UnknownCommand issue(ERS_HERE, command);
        ers::warning(issue);
        return;
      }
      
      std::vector<Registry::Entry> histos;

      //Collect all histograms beloning to this provider
      for(auto &flt : m_filters){
        Registry::instance().for_each(static_cast<std::function<void(const detail::Registry::Entry)> >(std::bind(&collect, std::placeholders::_1, std::ref(histos), flt)));
      }
          
      //Filter histograms for command regex, if provided
      if(not cmd.m_args.empty()){
        NameFilter filter(cmd.m_args[0]);
        auto it = histos.begin();
        while(it != histos.end()){
          if(not filter.matches((*it)->name())){
            it = histos.erase(it);
          } else {
            ++it;
          }
        }
      }

      for(auto &histo : histos){
        histo->command(*this, cmd);
      }
    }

    void OHCommander::execute(const Command& cmd, TH1* obj){
      switch (cmd.m_cmdtype){
      case commandtype::reset:
        obj->Reset();
        break;
      default:
        break;
      }
    }
  }
}

