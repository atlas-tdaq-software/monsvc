
#ifndef MONSVC_OHCOMMANDER_H_
#define MONSVC_OHCOMMANDER_H_

#include "oh/OHCommandListener.h"

#include "monsvc/NameFilter.h"
#include "monsvc/CommanderBase.h"

#include <unordered_map>
#include <string>
#include <memory>

namespace monsvc {

  namespace detail {
   
    /*
     * @brief The OHCommander implements command reception and execution via OH.
     * Only one commander should exists for each provider, otherwise commands
     * will be received multiple times.
     *
     * Currently the following commands are implemented:
     * 
     * *reset[;regex]* : resets the target histograms. If sent to a provider, 
     * rather than a histogram either all histograms will be reset or only 
     * those matching the optional regex
     *
     */
    class OHCommander: public OHCommandListener, public CommanderBase {
      
    public:
    OHCommander():OHCommandListener(),CommanderBase(){}
      ~OHCommander(){}
      
      using CommanderBase::execute;
      virtual void execute(const Command&, TH1 *);

      virtual void command(const std::string&, const std::string&);
      virtual void command(const std::string&);

      void addfilter(const NameFilter&);

    private:
      std::list<NameFilter> m_filters;
    };

    /*
      Returns a static map of weak pointer to the the existing commmanders.
      The weak pointers will be leaked, as nobody cleans the map. They are 
      not expected to be that many anyway.
    */
    std::unordered_map<std::string, std::weak_ptr<OHCommander>>&
      commanderstore();

    /* 
      Create an OH commander object if and only if it does already exists.
      The key is server_name+provide_name
    */
    std::shared_ptr<OHCommandListener> 
      create_commander(const std::string&, const std::string&, 
                       const NameFilter&);

  }
} /* namespace monsvc */
#endif /* MONSVC_OHCOMMANDER_H_ */
