#include "Registry.h"
#include <random>
#include <unistd.h>

namespace monsvc {

    namespace detail {
        static std::minstd_rand RANDOM_GENERATOR(::getpid());

        Registry::Registry()
        {}

        bool Registry::remove_object(const std::string& name, const Tag& tag)
        {
            std::unique_lock lock(m_mutex);
            const auto& range = m_objects.equal_range(name);
            const auto& found = 
                std::find_if(range.first, range.second, 
                             [tag](const std::pair<std::string, Entry>& e)
                             { return e.second->tag_info() == tag; });

            if (found != range.second) {
                m_objects.erase(found);
                return true;
            }
            return false;
        }

        int Registry::remove_objects(const std::string& name)
        {
            std::unique_lock lock(m_mutex);
            return m_objects.erase(name);
        }

        void Registry::clear()
        {
            // descriptors are deleted, referenced objects are not !
            std::unique_lock lock(m_mutex);
            m_objects.clear();
        }

        Registry& Registry::instance() {
            static Registry s_instance;
            return  s_instance;
        }

        void Registry::for_each(std::function<void(const Entry)> f) const
        {
            std::shared_lock lock(m_mutex);
            Storage::const_iterator it;
            for (it = m_objects.begin(); it != m_objects.end(); ++it) {
                f(it->second);
            }
        }

        void Registry::filter_shuffle_foreach(
              std::function<bool(const Entry)> filter
            , std::function<void(const Entry)> func
            , bool shuffle // default: true
            ) const
        {
            std::shared_lock lock(m_mutex);

            std::vector<Entry> selected;
            Storage::const_iterator itr;
            for (itr = m_objects.begin(); itr != m_objects.end(); ++itr) {
                if (filter(itr->second))
                    selected.push_back(itr->second);
            }

            if (shuffle)
                std::shuffle(selected.begin(), selected.end(), RANDOM_GENERATOR);

            std::vector<Entry>::const_iterator its;
            for (its = selected.begin(); its != selected.end(); ++its) {
                func(*its);
            }
        }

        void Registry::publish_object(const std::string& name, 
                                      Entry rep)
        {
            std::unique_lock lock(m_mutex);
            const auto& range = m_objects.equal_range(name);
            const Tag& requested_tag = rep->tag_info();
            
            bool found = 
                std::any_of(range.first, range.second, 
                            [requested_tag]
                            (const std::pair<std::string, Entry>& e)
                            { return e.second->tag_info() == requested_tag; });
            
            if (found) {
                std::string t = requested_tag ? 
                    std::to_string(requested_tag.value):std::string("None");
                throw NameExists(ERS_HERE, name, t);
            }

            m_objects.insert(std::make_pair(name, rep));
        }

        Registry::Entry 
        Registry::find_object(const std::string& name, const Tag& tag) const
        {
            std::shared_lock lock(m_mutex);
            const auto& range = m_objects.equal_range(name);
            const auto& found = 
                std::find_if(range.first, range.second, 
                             [tag](const std::pair<std::string, Entry>& e)
                             { return e.second->tag_info() == tag; });

            if (found == range.second) {
                std::string t = tag ? 
                    std::to_string(tag.value):std::string("None");
                throw NotFound(ERS_HERE, name, t);
            }
            return found->second;
        }

        std::vector<Registry::Entry>
        Registry::find_objects(const std::string& name) const
        {
            std::shared_lock lock(m_mutex);
            std::vector<Registry::Entry> result;
            
            const auto& range = m_objects.equal_range(name);
            for(auto it = range.first; it != range.second; ++it) {
                result.emplace_back(it->second);
            }
            return result;
        }

        bool Registry::remove_if(std::function<bool(Entry)> f)
        {
            std::unique_lock lock(m_mutex);
            for(Storage::iterator it = m_objects.begin(); it != m_objects.end(); ++it) {
                if (f(it->second)) {
                    m_objects.erase(it);
                    return true;
                }
            }
            return false;
        }

    }

}

/* Local Variables: */
/* c-basic-offset:4 */
/* End: */
