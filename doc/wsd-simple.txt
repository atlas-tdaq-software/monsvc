title monsvc Example Usage

MainApp->Library: initialize
activate MainApp

Library->MonitoringService: register object
note over Library: I'm done ;)

MainApp->PublishingController: start publishing
activate PublishingController
note over MainApp: goes on\n working

loop while not stopped
note over PublishingController:
    wait till 
    next update
end note

PublishingController->MonitoringService: give me all objects
MonitoringService->PublishingController: here you are
note over PublishingController:
   publish the objects
end note
end loop

MainApp->PublishingController: stop publishing
destroy PublishingController

Library->MonitoringService: remove object
