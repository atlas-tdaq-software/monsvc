#include "monsvc/MonitoringService.h"

#include <ctime>
#include <is/info.h>

/**
 * Test to check the overhead of our smart pointers.
 */

class Counter : public ISInfo {
public:
    ~Counter() {};

    void publishGuts(ISostream &s) {
        s << x;
    }
    void refreshGuts(ISistream &s) {
        s >> x;
    }

    std::ostream& print(std::ostream& os) const
    {
        return os << x;
    }

    int x;
};

static const int ITERATIONS = 100000000;

int main(int , char *[])
{
    Counter c;

    monsvc::ptr<Counter> pc(monsvc::MonitoringService::instance().register_object("name", &c));
    clock_t begin, end;
    double elapsed_secs;

    begin = clock();
    for (int i = 0; i < ITERATIONS; i++)
    {
        c.x++;
    }
    end = clock();
    elapsed_secs = double(end - begin);
    std::cerr << "Without locking: " << elapsed_secs << std::endl;

    // Takes about 50 times more
    begin = clock();
    for (int i = 0; i < ITERATIONS; i++)
    {
        pc->x++;
    }
    end = clock();
    elapsed_secs = double(end - begin);
    std::cerr << "With locking: " << elapsed_secs << std::endl;
}
