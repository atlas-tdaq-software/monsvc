#include "monsvc/ptr.h"
#include "PeriodicScheduler.h"
#include "monsvc/MonitoringService.h"
#include "monsvc/PublishingController.h"
#include "OHPublisher.h"
#include "ISPublisher.h"

#include <TH1.h>
#include <ipc/core.h>
#include <oh/OHRootProvider.h>
#include <is/infodictionary.h>

#include <boost/lexical_cast.hpp>

using namespace monsvc;
using namespace boost;
using namespace std;

template <typename T>
static T get_param(const std::string& name, const T default_val);

template <>
int get_param<int>(const string& name, const int default_val)
{
    char * param = getenv(name.c_str());
    try {
        if (param) {
            return lexical_cast<int>(param);
        }
    } catch (...) {
    }
    return default_val;
}

template <>
string get_param<string>(const string& name, const string default_val)
{
    char * param = getenv(name.c_str());
    if (param) {
        return string(param);
    }
    return default_val;
}

static const int HISTOGRAMS = get_param("HISTOGRAMS", 100);
static const int HISTOGRAM_SIZE = get_param("HISTOGRAM_SIZE", 1000);
static const int RULES = get_param("RULES", 1);
static const int TEST_DURATION = get_param("TEST_DURATION", 10);
static const int THREADS = get_param("THREADS", 1);

/*
export HISTOGRAMS=100
export HISTOGRAM_SIZE=10000
export RULES=1
export TEST_DURATION=20
 */
class TimerRule : public ConfigurationRule
{
public:
    TimerRule() : ConfigurationRule("_timer_rule", NameFilter("histo0"), 1, 1) {}

    class TimePublisher : public PublisherBase {
    public:
        TimePublisher() : t1(boost::posix_time::microsec_clock::local_time()), cnt(0) {}
        using PublisherBase::publish;
        void publish(const std::string&, TH1 *) {
            boost::posix_time::ptime t2 = boost::posix_time::microsec_clock::local_time();
            boost::posix_time::time_duration diff = t2 - t1;
            t1 = t2;
            ++cnt;
            cerr << cnt << ": " <<  diff << endl;
        }
        boost::posix_time::ptime t1;
        int cnt;
    };

    virtual std::shared_ptr<PublisherBase> create_publisher(const IPCPartition&,
            const std::string&) const {
        return std::make_shared<TimePublisher>();
    }

    virtual const std::string params_str() const { return "TimerPublisher()"; };

};

class Timer {
public:
    Timer(const std::string& msg) : msg(msg), begin(boost::posix_time::microsec_clock::local_time())
    {
    }
    ~Timer()
    {
        boost::posix_time::ptime end = boost::posix_time::microsec_clock::local_time();
        boost::posix_time::time_duration diff = end - begin;
        cerr << msg << diff << endl;
    }
    const std::string msg;
    boost::posix_time::ptime begin;
};

class ISHisto : public ISInfo {
public:
    ISHisto(int size, float min, float max) : m_size(size), data(size + 2)
    {
        double step = (max - min) / size;
        for (int i = 1; i <= size; i++) {
            data[i] = min + (i - 1) * step;
        }
    }
    int m_size;

    vector<float> data;
    void publishGuts(ISostream& os)
    {
        os << data;
    }

    void refreshGuts(ISistream& is)
    {
        is >> data;
    }

    std::ostream & print( std::ostream & os ) const {
        os << "size: " << m_size << std::endl;
        return os;
    }
};

class ISStressTest {
public:

    static string get_provider_name(int uniq)
    {
        stringstream ss;
        ss << "provider_name" << uniq;
        return ss.str();
    }

    ISStressTest()
        : partition(get_param<string>("TDAQ_PARTITION", "monsvc_test_part")),
          app_name(get_param<string>("TDAQ_APPLICATION_NAME", "monsvc_test_app")),
          histo("histo", "histo", HISTOGRAM_SIZE, 0, 100),
          names(),
          is_histo(HISTOGRAM_SIZE, 0, 100),
          is_small(1, 0, 100)
    {
      std::unique_ptr<double[]> content(new double[HISTOGRAM_SIZE]);
        for (int i = 0; i < HISTOGRAM_SIZE; i++) {
            content[i] = i * 100. / HISTOGRAM_SIZE;
        }
        histo.FillN(HISTOGRAM_SIZE, content.get(), content.get());
        for (int i = 0; i < HISTOGRAMS; i++)
        {
            names.push_back("histo" + boost::lexical_cast<string>(i));
            is_names.push_back("DF.ishisto" + boost::lexical_cast<string>(i));
        }

        for (int i = 0; i < HISTOGRAMS; i++)
        {
            MonitoringService::instance().register_object(names[i], &histo);
        }

        MonitoringService::instance().register_object("name", &histo);
        MonitoringService::instance().register_object("is_small", &is_small);
    }
    IPCPartition partition;
    string app_name;
    TH1F histo;
    vector<string> names;
    vector<string> is_names;
    ISHisto is_histo;
    ISHisto is_small;

    void test_throughput_multithreaded()
    {
        vector<std::shared_ptr<boost::thread> > threads;
        for (int i = 0; i < THREADS; i++)
        {
          threads.push_back(std::make_shared<boost::thread>(std::bind(&ISStressTest::test_throughput, this, i)));
        }
        for (int i = 0; i < THREADS; i++)
        {
            threads[i]->join();
        }
    }

    void test_throughput(int uniq) const
    {
        PublishingController pc(partition, app_name);
        TimerRule rule;
        pc.add_configuration_rule(rule);

        for (int i = 0; i < RULES; i++) {
            stringstream rule;
            rule << "rule" << i << ":.*/=>oh:(1,DF," << get_provider_name(uniq) <<")";
            pc.add_configuration_rule(*ConfigurationRule::from(rule.str()));
        }

        std::cout << "Start publishing" << std::endl;
        pc.start_publishing();

        boost::this_thread::sleep(boost::posix_time::seconds(TEST_DURATION));

        std::cout << "Stop publishing" << std::endl;
        pc.stop_publishing();
    }

    void test_raw_throughput_multithreaded()
    {
        Timer t("Raw duration total: ");
        vector<std::shared_ptr<boost::thread> > threads;
        for (int i = 0; i < THREADS; i++)
        {
          threads.push_back(std::make_shared<boost::thread>(std::bind(&ISStressTest::test_raw_throughput, this, i)));
        }
        for (int i = 0; i < THREADS; i++)
        {
            threads[i]->join();
        }
    }

    void test_raw_throughput(int uniq)
    {
        OHRootProvider p(partition, "DF", get_provider_name(uniq));
        {
            Timer t ("Raw duration: ");
            for (int i = 0; i < HISTOGRAMS; i++)
            {
                p.publish(histo, names[i]);
            }
        }
    }

    void test_is_throughput()
    {
        ISInfoDictionary dict(partition);
        {
            Timer t ("IS raw duration: ");
            for (int i = 0; i < HISTOGRAMS; i++)
            {
                dict.checkin(is_names[i], is_histo);
            }
        }
    }

    void test_publish_now_throughput_multithreaded()
    {
        Timer t("publish_now duration total: ");
        vector<std::shared_ptr<boost::thread> > threads;
        for (int i = 0; i < THREADS; i++)
        {
          threads.push_back(std::make_shared<boost::thread>(std::bind(&ISStressTest::test_publish_now_throughput, this, i)));
        }
        for (int i = 0; i < THREADS; i++)
        {
            threads[i]->join();
        }
    }

    void test_publish_now_is_throughput()
    {
        std::shared_ptr<ISPublisher> p(
                std::make_shared<ISPublisher>(partition, "DF", app_name));
        {
            Timer t("publish_now is_small duration: ");
            monsvc::detail::PeriodicScheduler::PeriodicTask task(p, NameFilter("is_small"));
            task.publish(0, 1);
        }
        {
            Timer t("raw is_small duration: ");
            ISInfoDictionary dict(partition);
            dict.checkin("DF.is_small_raw", is_small);
        }
    }

    void test_publish_now_throughput(int uniq)
    {
        std::shared_ptr<OHPublisher> p(
                std::make_shared<OHPublisher>(partition, "DF", get_provider_name(uniq)));
        {
            Timer t("publish_now duration: ");
            monsvc::detail::PeriodicScheduler::PeriodicTask task(p, NameFilter());
            task.publish(0, 1);
        }
    }

    void test_qos()
    {
        PublishingController pc(partition, app_name);
        TimerRule rule;
        pc.add_configuration_rule(rule);

        pc.add_configuration_rule(*ConfigurationRule::from("rule1:.*/=>oh:(3,DF,provider_name)"));
        pc.add_configuration_rule(*ConfigurationRule::from("rule2:name/=>oh:(1,DF,provider_name)"));

        std::cout << "Start publishing" << std::endl;
        pc.start_publishing();

        boost::this_thread::sleep(boost::posix_time::seconds(TEST_DURATION));

        std::cout << "Stop publishing" << std::endl;
        pc.stop_publishing();
    }

};



int main(int argc, char *argv[])
{
    IPCCore::init(argc, argv);
    ISStressTest test;

//    test.test_throughput_multithreaded();
//    test.test_publish_now_throughput();
//    test.test_publish_now_throughput_multithreaded();
    if (false) {
        test.test_is_throughput();
        test.test_raw_throughput_multithreaded();
    }

    test.test_publish_now_is_throughput();

}
