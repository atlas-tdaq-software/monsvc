#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE monsvc Scheduler Tests
#define BOOST_TEST_MAIN

#include <boost/test/unit_test.hpp>

#include "PeriodicScheduler.h"

#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/noncopyable.hpp>
#include <memory>
#include <boost/thread/barrier.hpp>
#include <boost/thread/thread.hpp>
#include <boost/thread/locks.hpp>

#include <map>
#include <iostream>

/**
 * Test suite for the task scheduler used to execute the periodic publishing.
 */

/*
 * Class that allows us to use a 'virtual' time when sleeping threads.
 *
 * NOTE: it is assumed that the tasks are executed within 1ms. If the test becomes flakey,
 * try increasing the m_sync_delay.
 */
class ConcurrentMockTimeManager : public monsvc::detail::PeriodicScheduler::TimeManager, boost::noncopyable {
public:
    ConcurrentMockTimeManager() : 
        m_time(0), m_mutex(), m_barriers(), m_sync_delay(1)
    {}

    void sleep(boost::posix_time::time_duration duration) override
    {
        std::shared_ptr<boost::barrier> barrier(std::make_shared<boost::barrier>(2));
        {
            std::lock_guard<std::mutex> lock(m_mutex);
            // Block current thread until the time advances by 'duration' seconds.
            m_barriers.insert(std::make_pair(m_time + duration.total_milliseconds(), barrier));
        }
        barrier->wait();
    }

    boost::posix_time::ptime now() override {
        static boost::gregorian::date reference_date(2012, boost::gregorian::Oct, 1);
        std::lock_guard<std::mutex> lock(m_mutex);
        return boost::posix_time::ptime(reference_date, boost::posix_time::milliseconds(m_time));
    }

    void pass_time(unsigned duration)
    {
        duration *= 1000;

        unsigned initial_time;
        {
            std::lock_guard<std::mutex> lock(m_mutex);
            initial_time = m_time;
        }

        wait_for_other_threads();
        std::multimap<unsigned, std::shared_ptr<boost::barrier> >::iterator it;
        while (!m_barriers.empty()) {
            std::shared_ptr<boost::barrier> barrier;
            {
                // Dequeue the first event.
                std::lock_guard<std::mutex> lock(m_mutex);
                it = m_barriers.begin();
                if (it->first > initial_time + duration) {
                    break;
                }
                m_time = it->first;
                barrier = it->second;
                m_barriers.erase(it);
            }

            // Wake the thread up and give it some time to execute its actions.
            barrier->wait();
            wait_for_other_threads();
        }
        {
            std::lock_guard<std::mutex> lock(m_mutex);
            m_time = initial_time + duration;
        }
        wait_for_other_threads();
    }

    // Give the other threads the opportunity to execute the tasks and go to sleep.
    void wait_for_other_threads() {
        boost::this_thread::sleep( boost::posix_time::milliseconds(m_sync_delay));
    }
private:
    unsigned m_time;
    std::mutex m_mutex;
    std::multimap<unsigned, std::shared_ptr<boost::barrier> > m_barriers;
    unsigned m_sync_delay;
};

class Task : public monsvc::detail::PeriodicScheduler::PeriodicTask {
public:
    Task() : 
        PeriodicTask(std::shared_ptr<monsvc::PublisherBase>(),
                     monsvc::NameFilter()) {}
    virtual ~Task() {}

    virtual void publish(unsigned slot, unsigned n) override {
        (void) n; //silence the compiler
        std::lock_guard<std::mutex> lock(mutex);
        calls[slot]++;
    }

    unsigned get_calls_no(unsigned slot = 0)
    {
        std::lock_guard<std::mutex> lock(mutex);
        return calls[slot];
    }
    
    std::map<unsigned, unsigned> get_calls_map()
    {
        std::lock_guard<std::mutex> lock(mutex);
        return calls;
    }

private:
    std::map<unsigned, unsigned> calls;
    std::mutex mutex;
};

class SchedulerTestFixture {
public:

    SchedulerTestFixture()
        : fake_time_manager(), time_manager(fake_time_manager), scheduler(fake_time_manager)
    {}

    static const unsigned interval = 3;
    ConcurrentMockTimeManager fake_time_manager;
    monsvc::detail::PeriodicScheduler::TimeManager& time_manager;
    monsvc::detail::PeriodicScheduler scheduler;
};


// NOTE: Defining the following checks as macros, we can know the line where they failed.

// Checks that the task is not running.
#define CHECK_RUNNING(task, interval)                           \
    do {                                                        \
        unsigned exec = 5;                                      \
        fake_time_manager.wait_for_other_threads();             \
        unsigned calls_no = task->get_calls_no(0);              \
        for (unsigned i = 0; i < exec; i++) {                   \
            fake_time_manager.pass_time(interval);              \
            BOOST_CHECK_EQUAL(task->get_calls_no(0), ++calls_no); \
        }                                                       \
    } while (false)

// Checks that the task is not running.
#define CHECK_NOT_RUNNING(task)                                 \
    do {                                                        \
        unsigned calls_no = task->get_calls_no(0);              \
        fake_time_manager.pass_time(100 * interval);            \
        BOOST_CHECK_EQUAL(task->get_calls_no(0), calls_no);     \
    } while (false)


#define CHECK_RUNNING_SLOT(task, interval, nslots)             \
    do {                                                       \
        unsigned exec = 5;                                     \
        fake_time_manager.wait_for_other_threads();            \
        auto calls_no = task->get_calls_map();                 \
        for (unsigned i = 0; i < exec; i++) {                  \
            fake_time_manager.pass_time(interval);             \
            auto calls_now = task->get_calls_map();            \
            for (unsigned int j = 0; j< nslots; j++) {         \
                BOOST_CHECK_EQUAL(calls_now[j], ++(calls_no[j]));\
            }                                                  \
        }                                                      \
    } while (false)


#define CHECK_NOT_RUNNING_SLOT(task, nslots)                  \
    do {                                                      \
        auto calls_no = task->get_calls_map();                \
        fake_time_manager.pass_time(100 * interval);          \
        auto calls_now = task->get_calls_map();               \
        for (unsigned int j = 0; j< nslots; j++) {            \
            BOOST_CHECK_EQUAL(calls_now[j], calls_no[j]);     \
        }                                                     \
    } while (false)



BOOST_FIXTURE_TEST_SUITE(SchedulerTest, SchedulerTestFixture)

BOOST_AUTO_TEST_CASE(test_scheduler_basic)
{
    
    std::shared_ptr<Task> task(std::make_shared<Task>());

    // Schedule the task.
    monsvc::detail::PeriodicScheduler::PeriodicTaskHandle handle1 = scheduler.schedule_task(task, interval, 1, false);

    // Before we start the scheduler, task is not executed.
    CHECK_NOT_RUNNING(task);

    // Start the scheduler and check that the task is running.
    scheduler.start();
    CHECK_RUNNING(task, interval);
    scheduler.stop();
    // After the scheduler is stopped, the task is not executed anymore.
    CHECK_NOT_RUNNING(task);

    // Restart the scheduler and check that the task is running again.
    scheduler.start();
    CHECK_RUNNING(task, interval);
    scheduler.stop();
    CHECK_NOT_RUNNING(task);

    // After we de-schedule the task, it is not run anymore.
    scheduler.deschedule_task(handle1);
    scheduler.start();
    CHECK_NOT_RUNNING(task);
    scheduler.stop();

}

BOOST_AUTO_TEST_CASE(test_scheduler_basic_slot)
{
    const unsigned interval = 11;
    const unsigned slots = 3;

    std::shared_ptr<Task> task(std::make_shared<Task>());

    // Schedule the task.
    monsvc::detail::PeriodicScheduler::PeriodicTaskHandle handle1 = scheduler.schedule_task(task, interval, slots, false);

    // Before we start the scheduler, task is not executed.
    CHECK_NOT_RUNNING_SLOT(task, slots);

    // Start the scheduler and check that the task is running.
    scheduler.start();
    CHECK_RUNNING_SLOT(task, interval, slots);
    scheduler.stop();
    // After the scheduler is stopped, the task is not executed anymore.
    CHECK_NOT_RUNNING_SLOT(task, slots);

    // Restart the scheduler and check that the task is running again.
    scheduler.start();
    CHECK_RUNNING_SLOT(task, interval, slots);
    scheduler.stop();
    CHECK_NOT_RUNNING_SLOT(task, slots);

    // After we de-schedule the task, it is not run anymore.
    scheduler.deschedule_task(handle1);
    scheduler.start();
    CHECK_NOT_RUNNING_SLOT(task, slots);
    scheduler.stop();

}

BOOST_AUTO_TEST_CASE(test_scheduler_advanced_slot)
{
    const unsigned interval = 11;
    const unsigned slots = 3;
    // slots --> 3666 3666 3668
    std::shared_ptr<Task> task(std::make_shared<Task>());

    // Schedule the task.
    monsvc::detail::PeriodicScheduler::PeriodicTaskHandle handle1 = scheduler.schedule_task(task, interval, slots, false);

    // Start the scheduler and check that the task is running.
    scheduler.start();
    fake_time_manager.wait_for_other_threads(); 
    BOOST_CHECK_EQUAL(task->get_calls_map()[0], 1);
    BOOST_CHECK_EQUAL(task->get_calls_map()[1], 0);
    BOOST_CHECK_EQUAL(task->get_calls_map()[2], 0);
    
    // 1 second
    fake_time_manager.pass_time(1);
    BOOST_CHECK_EQUAL(task->get_calls_map()[0], 1);
    BOOST_CHECK_EQUAL(task->get_calls_map()[1], 0);
    BOOST_CHECK_EQUAL(task->get_calls_map()[2], 0);

    // 2 seconds
    fake_time_manager.pass_time(1);
    BOOST_CHECK_EQUAL(task->get_calls_map()[0], 1);
    BOOST_CHECK_EQUAL(task->get_calls_map()[1], 0);
    BOOST_CHECK_EQUAL(task->get_calls_map()[2], 0);

    // 3 seconds
    fake_time_manager.pass_time(1);
    BOOST_CHECK_EQUAL(task->get_calls_map()[0], 1);
    BOOST_CHECK_EQUAL(task->get_calls_map()[1], 0);
    BOOST_CHECK_EQUAL(task->get_calls_map()[2], 0);

    // 4 seconds
    fake_time_manager.pass_time(1);
    BOOST_CHECK_EQUAL(task->get_calls_map()[0], 1);
    BOOST_CHECK_EQUAL(task->get_calls_map()[1], 1);
    BOOST_CHECK_EQUAL(task->get_calls_map()[2], 0);

    // 7 seconds
    fake_time_manager.pass_time(3);
    BOOST_CHECK_EQUAL(task->get_calls_map()[0], 1);
    BOOST_CHECK_EQUAL(task->get_calls_map()[1], 1);
    BOOST_CHECK_EQUAL(task->get_calls_map()[2], 0);
    
    // 8 seconds
    fake_time_manager.pass_time(1);
    BOOST_CHECK_EQUAL(task->get_calls_map()[0], 1);
    BOOST_CHECK_EQUAL(task->get_calls_map()[1], 1);
    BOOST_CHECK_EQUAL(task->get_calls_map()[2], 1);
    
    // 10 seconds
    fake_time_manager.pass_time(2);
    BOOST_CHECK_EQUAL(task->get_calls_map()[0], 1);
    BOOST_CHECK_EQUAL(task->get_calls_map()[1], 1);
    BOOST_CHECK_EQUAL(task->get_calls_map()[2], 1);
    
    // 11 seconds
    fake_time_manager.pass_time(1);
    BOOST_CHECK_EQUAL(task->get_calls_map()[0], 2);
    BOOST_CHECK_EQUAL(task->get_calls_map()[1], 1);
    BOOST_CHECK_EQUAL(task->get_calls_map()[2], 1);

    scheduler.stop();
    scheduler.deschedule_task(handle1);
}

BOOST_AUTO_TEST_CASE(test_scheduler_state)
{
    std::shared_ptr<Task> task(std::make_shared<Task>());

    // Schedule the task.
    scheduler.schedule_task(task, interval, 1, false);

    scheduler.start();
    CHECK_RUNNING(task, interval);
    // If we start the scheduler again, nothing happens; the task executes as before.
    BOOST_CHECK_NO_THROW(scheduler.start());
    CHECK_RUNNING(task, interval);
    // The first stop, stops the tasks.
    scheduler.stop();
    CHECK_NOT_RUNNING(task);
    // A further stop has no effect.
    BOOST_CHECK_NO_THROW(scheduler.stop());
}


BOOST_AUTO_TEST_CASE(test_schedule_while_running)
{
    std::shared_ptr<Task> task1(std::make_shared<Task>());
    unsigned interval1 = 313;

    std::shared_ptr<Task> task2(std::make_shared<Task>());
    unsigned interval2 = 317;

    scheduler.start();
    // At the beginning everything is stopped.
    CHECK_NOT_RUNNING(task1);
    CHECK_NOT_RUNNING(task2);

    // We start task 1.
    monsvc::detail::PeriodicScheduler::PeriodicTaskHandle h1 = scheduler.schedule_task(task1, interval1, 1, false);
    CHECK_RUNNING(task1, interval1);
    CHECK_NOT_RUNNING(task2);

    // We start task 2.
    monsvc::detail::PeriodicScheduler::PeriodicTaskHandle h2 = scheduler.schedule_task(task2, interval2, 1, false);
    CHECK_RUNNING(task2, interval2);
    CHECK_RUNNING(task1, interval1);

    // We stop task 1.
    scheduler.deschedule_task(h1);
    CHECK_NOT_RUNNING(task1);
    CHECK_RUNNING(task2, interval2);

    // We stop task 2.
    scheduler.deschedule_task(h2);
    CHECK_NOT_RUNNING(task1);
    CHECK_NOT_RUNNING(task2);

    scheduler.stop();
}


BOOST_AUTO_TEST_CASE(test_schedule_multiple_tasks)
{
    std::shared_ptr<Task> task1(std::make_shared<Task>());
    unsigned interval1 = 1;

    std::shared_ptr<Task> task2(std::make_shared<Task>());
    unsigned interval2 = 2;

    std::shared_ptr<Task> task3(std::make_shared<Task>());
    unsigned interval3 = 3;

    // tasks 1, 3 start from second 0.
    scheduler.schedule_task(task1, interval1, 1, false);
    scheduler.schedule_task(task3, interval3, 1, false);


    // 0 second
    scheduler.start();
    fake_time_manager.wait_for_other_threads();
    BOOST_CHECK_EQUAL(task1->get_calls_no(), 1);
    BOOST_CHECK_EQUAL(task2->get_calls_no(), 0);
    BOOST_CHECK_EQUAL(task3->get_calls_no(), 1);

    // 1st second
    fake_time_manager.pass_time(1);
    scheduler.schedule_task(task2, interval2, 1, false);
    fake_time_manager.wait_for_other_threads();
    BOOST_CHECK_EQUAL(task1->get_calls_no(), 2);
    BOOST_CHECK_EQUAL(task2->get_calls_no(), 1);
    BOOST_CHECK_EQUAL(task3->get_calls_no(), 1);

    // 2nd second
    fake_time_manager.pass_time(1);
    BOOST_CHECK_EQUAL(task1->get_calls_no(), 3);
    BOOST_CHECK_EQUAL(task2->get_calls_no(), 1);
    BOOST_CHECK_EQUAL(task3->get_calls_no(), 1);

    // 3rd second
    fake_time_manager.pass_time(1);
    BOOST_CHECK_EQUAL(task1->get_calls_no(), 4);
    BOOST_CHECK_EQUAL(task2->get_calls_no(), 2);
    BOOST_CHECK_EQUAL(task3->get_calls_no(), 2);

    // 4th second
    fake_time_manager.pass_time(1);
    BOOST_CHECK_EQUAL(task1->get_calls_no(), 5);
    BOOST_CHECK_EQUAL(task2->get_calls_no(), 2);
    BOOST_CHECK_EQUAL(task3->get_calls_no(), 2);

    // 5th second
    fake_time_manager.pass_time(1);
    BOOST_CHECK_EQUAL(task1->get_calls_no(), 6);
    BOOST_CHECK_EQUAL(task2->get_calls_no(), 3);
    BOOST_CHECK_EQUAL(task3->get_calls_no(), 2);

    // 6th second
    fake_time_manager.pass_time(1);
    BOOST_CHECK_EQUAL(task1->get_calls_no(), 7);
    BOOST_CHECK_EQUAL(task2->get_calls_no(), 3);
    BOOST_CHECK_EQUAL(task3->get_calls_no(), 3);

    scheduler.stop();
}

BOOST_AUTO_TEST_CASE(test_schedule_multiple_tasks_slot)
{
    std::shared_ptr<Task> task1(std::make_shared<Task>());
    unsigned interval1 = 11;
    unsigned slots1 = 3;

    std::shared_ptr<Task> task2(std::make_shared<Task>());
    unsigned interval2 = 5;
    unsigned slots2 = 1;
    
    std::shared_ptr<Task> task3(std::make_shared<Task>());
    unsigned interval3 = 8;
    unsigned slots3 = 2;

    // tasks 1, 3 start from second 0.
    scheduler.schedule_task(task1, interval1, slots1, false);
    scheduler.schedule_task(task3, interval3, slots3, false);

    // 0 second
    scheduler.start();
    fake_time_manager.wait_for_other_threads();
    BOOST_CHECK_EQUAL(task1->get_calls_map()[0], 1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[1], 0);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[2], 0);
    
    BOOST_CHECK_EQUAL(task2->get_calls_no(), 0);
    
    BOOST_CHECK_EQUAL(task3->get_calls_map()[0], 1);
    BOOST_CHECK_EQUAL(task3->get_calls_map()[1], 0);

    // 1st second
    fake_time_manager.pass_time(1);
    scheduler.schedule_task(task2, interval2, slots2, false);
    fake_time_manager.wait_for_other_threads();
    BOOST_CHECK_EQUAL(task1->get_calls_map()[0], 1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[1], 0);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[2], 0);
    
    BOOST_CHECK_EQUAL(task2->get_calls_no(), 1);

    BOOST_CHECK_EQUAL(task3->get_calls_map()[0], 1);
    BOOST_CHECK_EQUAL(task3->get_calls_map()[1], 0);

    // 2nd second
    fake_time_manager.pass_time(1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[0], 1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[1], 0);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[2], 0);

    BOOST_CHECK_EQUAL(task2->get_calls_no(), 1);

    BOOST_CHECK_EQUAL(task3->get_calls_map()[0], 1);
    BOOST_CHECK_EQUAL(task3->get_calls_map()[1], 0);

    // 3rd second
    fake_time_manager.pass_time(1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[0], 1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[1], 0);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[2], 0);

    BOOST_CHECK_EQUAL(task2->get_calls_no(), 1);

    BOOST_CHECK_EQUAL(task3->get_calls_map()[0], 1);
    BOOST_CHECK_EQUAL(task3->get_calls_map()[1], 0);

    // 4th second
    fake_time_manager.pass_time(1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[0], 1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[1], 1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[2], 0);

    BOOST_CHECK_EQUAL(task2->get_calls_no(), 1);
    
    BOOST_CHECK_EQUAL(task3->get_calls_map()[0], 1);
    BOOST_CHECK_EQUAL(task3->get_calls_map()[1], 1);

    // 5th second
    fake_time_manager.pass_time(1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[0], 1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[1], 1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[2], 0);

    BOOST_CHECK_EQUAL(task2->get_calls_no(), 1);
    
    BOOST_CHECK_EQUAL(task3->get_calls_map()[0], 1);
    BOOST_CHECK_EQUAL(task3->get_calls_map()[1], 1);

    // 6th second
    fake_time_manager.pass_time(1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[0], 1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[1], 1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[2], 0);

    BOOST_CHECK_EQUAL(task2->get_calls_no(), 2);
    
    BOOST_CHECK_EQUAL(task3->get_calls_map()[0], 1);
    BOOST_CHECK_EQUAL(task3->get_calls_map()[1], 1);

    // 7th second
    fake_time_manager.pass_time(1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[0], 1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[1], 1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[2], 0);

    BOOST_CHECK_EQUAL(task2->get_calls_no(), 2);

    BOOST_CHECK_EQUAL(task3->get_calls_map()[0], 1);
    BOOST_CHECK_EQUAL(task3->get_calls_map()[1], 1);
    
    // 8th second
    fake_time_manager.pass_time(1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[0], 1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[1], 1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[2], 1);

    BOOST_CHECK_EQUAL(task2->get_calls_no(), 2);

    BOOST_CHECK_EQUAL(task3->get_calls_map()[0], 2);
    BOOST_CHECK_EQUAL(task3->get_calls_map()[1], 1);

    // 9th second
    fake_time_manager.pass_time(1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[0], 1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[1], 1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[2], 1);

    BOOST_CHECK_EQUAL(task2->get_calls_no(), 2);

    BOOST_CHECK_EQUAL(task3->get_calls_map()[0], 2);
    BOOST_CHECK_EQUAL(task3->get_calls_map()[1], 1);

    // 10th second
    fake_time_manager.pass_time(1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[0], 1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[1], 1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[2], 1);

    BOOST_CHECK_EQUAL(task2->get_calls_no(), 2);

    BOOST_CHECK_EQUAL(task3->get_calls_map()[0], 2);
    BOOST_CHECK_EQUAL(task3->get_calls_map()[1], 1);

    // 11th second
    fake_time_manager.pass_time(1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[0], 2);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[1], 1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[2], 1);

    BOOST_CHECK_EQUAL(task2->get_calls_no(), 3);

    BOOST_CHECK_EQUAL(task3->get_calls_map()[0], 2);
    BOOST_CHECK_EQUAL(task3->get_calls_map()[1], 1);
    
    scheduler.stop();

}

BOOST_AUTO_TEST_CASE(test_schedule_same_interval)
{
    std::shared_ptr<Task> task1(std::make_shared<Task>());
    std::shared_ptr<Task> task2(std::make_shared<Task>());
    std::shared_ptr<Task> task3(std::make_shared<Task>());

    unsigned interval = 100;

    scheduler.schedule_task(task1, interval, 1, false);
    scheduler.start();

    // Second 10, task 2 started, but not executed until second 100.
    fake_time_manager.pass_time(10);
    scheduler.schedule_task(task2, interval, 1, false);
    fake_time_manager.wait_for_other_threads();

    BOOST_CHECK_EQUAL(task1->get_calls_no(), 1);
    BOOST_CHECK_EQUAL(task2->get_calls_no(), 0);
    BOOST_CHECK_EQUAL(task3->get_calls_no(), 0);

    // Second 90, task 3 started, but not executed until second 100.
    fake_time_manager.pass_time(80);
    scheduler.schedule_task(task3, interval, 1, false);
    fake_time_manager.wait_for_other_threads();

    BOOST_CHECK_EQUAL(task1->get_calls_no(), 1);
    BOOST_CHECK_EQUAL(task2->get_calls_no(), 0);
    BOOST_CHECK_EQUAL(task3->get_calls_no(), 0);

    // Second 100, all tasks are executed.
    fake_time_manager.pass_time(10);

    BOOST_CHECK_EQUAL(task1->get_calls_no(), 2);
    BOOST_CHECK_EQUAL(task2->get_calls_no(), 1);
    BOOST_CHECK_EQUAL(task3->get_calls_no(), 1);

    scheduler.stop();
}

BOOST_AUTO_TEST_CASE(test_schedule_same_interval_slot)
{
    std::shared_ptr<Task> task1(std::make_shared<Task>());
    std::shared_ptr<Task> task2(std::make_shared<Task>());
    std::shared_ptr<Task> task3(std::make_shared<Task>());
    
    unsigned interval = 11;
    unsigned slots = 3;

    // tasks 1 start from second 0.
    scheduler.schedule_task(task1, interval, slots, false);
    
    // 0 second
    scheduler.start();
    fake_time_manager.wait_for_other_threads();
    BOOST_CHECK_EQUAL(task1->get_calls_map()[0], 1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[1], 0);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[2], 0);
    
    BOOST_CHECK_EQUAL(task2->get_calls_map()[0], 0);
    BOOST_CHECK_EQUAL(task2->get_calls_map()[1], 0);
    BOOST_CHECK_EQUAL(task2->get_calls_map()[2], 0);
    
    BOOST_CHECK_EQUAL(task3->get_calls_map()[0], 0);
    BOOST_CHECK_EQUAL(task3->get_calls_map()[1], 0);
    BOOST_CHECK_EQUAL(task3->get_calls_map()[2], 0);

    // 1st second
    fake_time_manager.pass_time(1);
    // Task2 will be active at the next slot (3.66)
    scheduler.schedule_task(task2, interval, slots, false);
    fake_time_manager.wait_for_other_threads();
    BOOST_CHECK_EQUAL(task1->get_calls_map()[0], 1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[1], 0);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[2], 0);
    
    BOOST_CHECK_EQUAL(task2->get_calls_map()[0], 0);
    BOOST_CHECK_EQUAL(task2->get_calls_map()[1], 0);
    BOOST_CHECK_EQUAL(task2->get_calls_map()[2], 0);
    
    BOOST_CHECK_EQUAL(task3->get_calls_map()[0], 0);
    BOOST_CHECK_EQUAL(task3->get_calls_map()[1], 0);
    BOOST_CHECK_EQUAL(task3->get_calls_map()[2], 0);
    
    // 2nd second
    fake_time_manager.pass_time(1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[0], 1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[1], 0);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[2], 0);
    
    BOOST_CHECK_EQUAL(task2->get_calls_map()[0], 0);
    BOOST_CHECK_EQUAL(task2->get_calls_map()[1], 0);
    BOOST_CHECK_EQUAL(task2->get_calls_map()[2], 0);
    
    BOOST_CHECK_EQUAL(task3->get_calls_map()[0], 0);
    BOOST_CHECK_EQUAL(task3->get_calls_map()[1], 0);
    BOOST_CHECK_EQUAL(task3->get_calls_map()[2], 0);

    // 3rd second
    fake_time_manager.pass_time(1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[0], 1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[1], 0);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[2], 0);

    BOOST_CHECK_EQUAL(task2->get_calls_map()[0], 0);
    BOOST_CHECK_EQUAL(task2->get_calls_map()[1], 0);
    BOOST_CHECK_EQUAL(task2->get_calls_map()[2], 0);
    
    BOOST_CHECK_EQUAL(task3->get_calls_map()[0], 0);
    BOOST_CHECK_EQUAL(task3->get_calls_map()[1], 0);
    BOOST_CHECK_EQUAL(task3->get_calls_map()[2], 0);

    // 4th second
    fake_time_manager.pass_time(1);
    // Task3 will be active at the next slot (7.32)
    scheduler.schedule_task(task3, interval, slots, false);
    fake_time_manager.wait_for_other_threads();
    BOOST_CHECK_EQUAL(task1->get_calls_map()[0], 1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[1], 1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[2], 0);

    BOOST_CHECK_EQUAL(task2->get_calls_map()[0], 0);
    BOOST_CHECK_EQUAL(task2->get_calls_map()[1], 1);
    BOOST_CHECK_EQUAL(task2->get_calls_map()[2], 0);

    BOOST_CHECK_EQUAL(task3->get_calls_map()[0], 0);
    BOOST_CHECK_EQUAL(task3->get_calls_map()[1], 0);
    BOOST_CHECK_EQUAL(task3->get_calls_map()[2], 0);

    // 5th second
    fake_time_manager.pass_time(1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[0], 1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[1], 1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[2], 0);

    BOOST_CHECK_EQUAL(task2->get_calls_map()[0], 0);
    BOOST_CHECK_EQUAL(task2->get_calls_map()[1], 1);
    BOOST_CHECK_EQUAL(task2->get_calls_map()[2], 0);

    BOOST_CHECK_EQUAL(task3->get_calls_map()[0], 0);
    BOOST_CHECK_EQUAL(task3->get_calls_map()[1], 0);
    BOOST_CHECK_EQUAL(task3->get_calls_map()[2], 0);

    // 6th second
    fake_time_manager.pass_time(1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[0], 1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[1], 1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[2], 0);

    BOOST_CHECK_EQUAL(task2->get_calls_map()[0], 0);
    BOOST_CHECK_EQUAL(task2->get_calls_map()[1], 1);
    BOOST_CHECK_EQUAL(task2->get_calls_map()[2], 0);

    BOOST_CHECK_EQUAL(task3->get_calls_map()[0], 0);
    BOOST_CHECK_EQUAL(task3->get_calls_map()[1], 0);
    BOOST_CHECK_EQUAL(task3->get_calls_map()[2], 0);

    // 7th second
    fake_time_manager.pass_time(1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[0], 1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[1], 1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[2], 0);

    BOOST_CHECK_EQUAL(task2->get_calls_map()[0], 0);
    BOOST_CHECK_EQUAL(task2->get_calls_map()[1], 1);
    BOOST_CHECK_EQUAL(task2->get_calls_map()[2], 0);

    BOOST_CHECK_EQUAL(task3->get_calls_map()[0], 0);
    BOOST_CHECK_EQUAL(task3->get_calls_map()[1], 0);
    BOOST_CHECK_EQUAL(task3->get_calls_map()[2], 0);
    
    // 8th second
    fake_time_manager.pass_time(1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[0], 1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[1], 1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[2], 1);

    BOOST_CHECK_EQUAL(task2->get_calls_map()[0], 0);
    BOOST_CHECK_EQUAL(task2->get_calls_map()[1], 1);
    BOOST_CHECK_EQUAL(task2->get_calls_map()[2], 1);

    BOOST_CHECK_EQUAL(task3->get_calls_map()[0], 0);
    BOOST_CHECK_EQUAL(task3->get_calls_map()[1], 0);
    BOOST_CHECK_EQUAL(task3->get_calls_map()[2], 1);


    // 9th second
    fake_time_manager.pass_time(1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[0], 1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[1], 1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[2], 1);

    BOOST_CHECK_EQUAL(task2->get_calls_map()[0], 0);
    BOOST_CHECK_EQUAL(task2->get_calls_map()[1], 1);
    BOOST_CHECK_EQUAL(task2->get_calls_map()[2], 1);

    BOOST_CHECK_EQUAL(task3->get_calls_map()[0], 0);
    BOOST_CHECK_EQUAL(task3->get_calls_map()[1], 0);
    BOOST_CHECK_EQUAL(task3->get_calls_map()[2], 1);

    // 10th second
    fake_time_manager.pass_time(1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[0], 1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[1], 1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[2], 1);

    BOOST_CHECK_EQUAL(task2->get_calls_map()[0], 0);
    BOOST_CHECK_EQUAL(task2->get_calls_map()[1], 1);
    BOOST_CHECK_EQUAL(task2->get_calls_map()[2], 1);

    BOOST_CHECK_EQUAL(task3->get_calls_map()[0], 0);
    BOOST_CHECK_EQUAL(task3->get_calls_map()[1], 0);
    BOOST_CHECK_EQUAL(task3->get_calls_map()[2], 1);

    // 11th second
    fake_time_manager.pass_time(1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[0], 2);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[1], 1);
    BOOST_CHECK_EQUAL(task1->get_calls_map()[2], 1);
    
    BOOST_CHECK_EQUAL(task2->get_calls_map()[0], 1);
    BOOST_CHECK_EQUAL(task2->get_calls_map()[1], 1);
    BOOST_CHECK_EQUAL(task2->get_calls_map()[2], 1);

    BOOST_CHECK_EQUAL(task3->get_calls_map()[0], 1);
    BOOST_CHECK_EQUAL(task3->get_calls_map()[1], 0);
    BOOST_CHECK_EQUAL(task3->get_calls_map()[2], 1);

    scheduler.stop();
}

BOOST_AUTO_TEST_CASE(test_absolute_time_sync)
{
    std::shared_ptr<Task> task1(std::make_shared<Task>());
    std::shared_ptr<Task> task2(std::make_shared<Task>());
    std::shared_ptr<Task> task3(std::make_shared<Task>());

    // Schedule the tasks
    monsvc::detail::PeriodicScheduler::PeriodicTaskHandle handle1 = scheduler.schedule_task(task1, 3, 1, true);
    monsvc::detail::PeriodicScheduler::PeriodicTaskHandle handle2 = scheduler.schedule_task(task2, 5, 1, true);
    monsvc::detail::PeriodicScheduler::PeriodicTaskHandle handle3 = scheduler.schedule_task(task3, 1, 1, true);

    // Advance of 1 seconds, so that we will need to sleep in order to sync
    fake_time_manager.pass_time(1);
  
    scheduler.start();
    fake_time_manager.wait_for_other_threads();
    BOOST_CHECK_EQUAL(task1->get_calls_no(), 0);
    BOOST_CHECK_EQUAL(task2->get_calls_no(), 0);
    BOOST_CHECK_EQUAL(task3->get_calls_no(), 0);
  
    fake_time_manager.pass_time(1);
    BOOST_CHECK_EQUAL(task1->get_calls_no(), 0);
    BOOST_CHECK_EQUAL(task2->get_calls_no(), 0);
    BOOST_CHECK_EQUAL(task3->get_calls_no(), 1);

    fake_time_manager.pass_time(1);
    BOOST_CHECK_EQUAL(task1->get_calls_no(), 1);
    BOOST_CHECK_EQUAL(task2->get_calls_no(), 0);
    BOOST_CHECK_EQUAL(task3->get_calls_no(), 2);

    fake_time_manager.pass_time(1);
    BOOST_CHECK_EQUAL(task1->get_calls_no(), 1);
    BOOST_CHECK_EQUAL(task2->get_calls_no(), 0);
    BOOST_CHECK_EQUAL(task3->get_calls_no(), 3);

    fake_time_manager.pass_time(1);
    BOOST_CHECK_EQUAL(task1->get_calls_no(), 1);
    BOOST_CHECK_EQUAL(task2->get_calls_no(), 1);
    BOOST_CHECK_EQUAL(task3->get_calls_no(), 4);

    scheduler.stop();
    scheduler.deschedule_task(handle1);
    scheduler.deschedule_task(handle2);
    scheduler.deschedule_task(handle3);
}

BOOST_AUTO_TEST_SUITE_END()

/* Local Variables: */
/* c-basic-offset:4 */
/* End: */
