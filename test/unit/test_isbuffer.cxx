#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE monsvc ISBuffer Tests
#define BOOST_TEST_MAIN

#include <boost/test/unit_test.hpp>

#include "test/unit/test_online_fixture.h"

#include "monsvc/TISBuffer.h"

#include <TObject.h>
#include <is/infodictionary.h>
#include <is/infoiterator.h>
#include <is/criteria.h>

#include <string>
#include <vector>

/**
 * Test suite for the TObject to ISInfo conversion by using an ISBuffer.
 * XXX: need not use the IS server. Need just to check that the resulting object is OK.
 */

class ISInfoStreamer {
public:
    virtual void StreamInfo(TBuffer& ) {}
    virtual ~ISInfoStreamer() {}
};

//
// A class that derives from TObject
// No dictionary is needed, but a custom StreamInfo method.
//
class MyObject : public TObject, public ISInfoStreamer {
public:
    MyObject()
        : x(1),
          y(2.0),
          d(3.0),
          c(20, 6.0),
          s("7.0")
    {
        for(int i = 0; i< 10; i++) a[i] = i * 4;
        for(int i = 0; i< 20; i++) b[i] = i * 5;
    }

    void StreamInfo(TBuffer& buf)
    {
        if(buf.IsWriting()) {
            buf << x << y << d;
            buf.WriteArray(a, 10);
            buf.WriteArray(b, 20);
            buf.WriteArray(&c[0], c.size());
            buf.WriteString(s.c_str());
        }
    }

public:
    int x;     // x
    float y;   // y
    double d;  // d

    int a[10];
    float b[20];

    std::vector<double> c;

    std::string s;
};


class TInfo : public ISInfo {
public:
    TInfo(TObject *obj)
        : ISInfo(obj->ClassName()),
          m_obj(obj)
    {}

    void publishGuts(ISostream& s)
    {
        if(ISInfoStreamer *stream = dynamic_cast<ISInfoStreamer*>(m_obj)) {
            monsvc::TISBuffer buffer(s);
            stream->StreamInfo(buffer);
        }
    }

    void refreshGuts(ISistream& )
    {
    }

private:
    TObject *m_obj;
};


BOOST_FIXTURE_TEST_SUITE(ISBufferTest, OnlineTestFixture)

// XXX: test the size of the datatypes (statically).
BOOST_AUTO_TEST_CASE(check_isbuffer)
{
    using namespace std;
    using namespace monsvc;

    std::string obj_name = get_server_name() + ".MyObject";

    MyObject obj;
    TInfo info(&obj);
    get_is_dictionary().checkin(obj_name, info);


    ISInfoAny isa;
    get_is_dictionary().findValue(obj_name,isa);

    // Check that we retrieve the same object.
    BOOST_REQUIRE_EQUAL(isa.type().name(), "TObject"); // XXX: The class name is lost
    BOOST_REQUIRE_EQUAL(isa.countAttributes(), 7);
    
    // First attribute is an integer;
    BOOST_REQUIRE_EQUAL(isa.getAttributeType(), ISType::S32);
    int x;
    isa >> x;
    BOOST_CHECK_EQUAL(x, 1);
    
    BOOST_REQUIRE_EQUAL(isa.getAttributeType(), ISType::Float);
    float y;
    isa >> y;
    BOOST_CHECK_EQUAL(y, 2.0);
    
    BOOST_REQUIRE_EQUAL(isa.getAttributeType(), ISType::Double);
    double d;
    isa >> d;
    BOOST_CHECK_EQUAL(d, 3.0);
    
    // FIXME : array members are not published
    
    BOOST_REQUIRE_EQUAL(isa.isAttributeArray(), true);
    BOOST_REQUIRE_EQUAL(isa.getAttributeType(), ISType::S32);
    int a[10];
    size_t size = 10;
    isa.get(&a[0], size);
    BOOST_CHECK_EQUAL(size, 10);
    for (int i = 0; i < 10; i++) {
      BOOST_CHECK_EQUAL(a[i], 4 * i);
    }
    
    BOOST_REQUIRE_EQUAL(isa.isAttributeArray(), true);
    BOOST_REQUIRE_EQUAL(isa.getAttributeType(), ISType::Float);
    float b[20];
    size = 20;
    isa.get(&b[0], size);
    BOOST_CHECK_EQUAL(size, 20);
    for (int i = 0; i < 20; i++) {
      BOOST_CHECK_EQUAL(b[i], 5.0 * i);
    }
    

    BOOST_REQUIRE_EQUAL(isa.isAttributeArray(), true);
    BOOST_REQUIRE_EQUAL(isa.getAttributeType(), ISType::Double);
    double c[20];
    size = 20;
    isa.get(&c[0], size);
    BOOST_CHECK_EQUAL(size, 20);
    for (int i = 0; i < 20; i++) {
      BOOST_CHECK_EQUAL(c[i], 6.0);
    }
    
    BOOST_REQUIRE_EQUAL(isa.getAttributeType(), ISType::String);
    std::string s;
    isa >> s;
    BOOST_CHECK_EQUAL(s, "7.0");

    /*bool found = false;
    ISInfoIterator ii(get_partition(), get_server_name(), ISCriteria(".*"));
    while( ii() ) {
        if (ii.name() == obj_name) {
            found = true;
            ISInfoAny isa;
            ii.value( isa );

            // Check that we retrieve the same object.
            BOOST_REQUIRE_EQUAL(isa.type().name(), "TObject"); // XXX: The class name is lost
            BOOST_REQUIRE_EQUAL(isa.countAttributes(), 7);

            // First attribute is an integer;
            BOOST_REQUIRE_EQUAL(isa.getAttributeType(), ISType::S32);
            int x;
            isa >> x;
            BOOST_CHECK_EQUAL(x, 1);

            BOOST_REQUIRE_EQUAL(isa.getAttributeType(), ISType::Float);
            float y;
            isa >> y;
            BOOST_CHECK_EQUAL(y, 2.0);

            BOOST_REQUIRE_EQUAL(isa.getAttributeType(), ISType::Double);
            double d;
            isa >> d;
            BOOST_CHECK_EQUAL(d, 3.0);

            // FIXME : array members are not published

            BOOST_REQUIRE_EQUAL(isa.isAttributeArray(), true);
            BOOST_REQUIRE_EQUAL(isa.getAttributeType(), ISType::S32);
            int a[10];
            size_t size = 10;
            isa.get(&a[0], size);
            BOOST_CHECK_EQUAL(size, 10);
            for (int i = 0; i < 10; i++) {
                BOOST_CHECK_EQUAL(a[i], 4 * i);
            }

            BOOST_REQUIRE_EQUAL(isa.isAttributeArray(), true);
            BOOST_REQUIRE_EQUAL(isa.getAttributeType(), ISType::Float);
            float b[20];
            size = 20;
            isa.get(&b[0], size);
            BOOST_CHECK_EQUAL(size, 20);
            for (int i = 0; i < 20; i++) {
                BOOST_CHECK_EQUAL(b[i], 5.0 * i);
            }


            BOOST_REQUIRE_EQUAL(isa.isAttributeArray(), true);
            BOOST_REQUIRE_EQUAL(isa.getAttributeType(), ISType::Double);
            double c[20];
            size = 20;
            isa.get(&c[0], size);
            BOOST_CHECK_EQUAL(size, 20);
            for (int i = 0; i < 20; i++) {
                BOOST_CHECK_EQUAL(c[i], 6.0);
            }

            BOOST_REQUIRE_EQUAL(isa.getAttributeType(), ISType::String);
            std::string s;
            isa >> s;
            BOOST_CHECK_EQUAL(s, "7.0");
        }
    }
    BOOST_CHECK_EQUAL(found, true);
    */
}

BOOST_AUTO_TEST_SUITE_END()
