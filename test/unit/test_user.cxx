#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE monsvc MonitoringService Tests
#define BOOST_TEST_MAIN

#include <boost/test/unit_test.hpp>

#include "monsvc/ptr.h"
#include "monsvc/MonitoringService.h"
#include "Registry.h"

#include <boost/bind/bind.hpp>
#include <TObject.h>
#include <TGraph.h>
#include <TGraph2D.h>
#include <TH1.h>
#include <is/info.h>

/**
 * Test suite for the MonitoringService interface.
 */

// Dummy concrete ISInfo class.
class TestInfo : public ISInfo {
public:
    void publishGuts(ISostream &) {}
    void refreshGuts(ISistream &) {}
    std::ostream& print(std::ostream& os) const { return os; }
};


class MonitoringServiceFixture {
public:
    static monsvc::MonitoringService& get_service() {
        return monsvc::MonitoringService::instance();
    }

    ~MonitoringServiceFixture() {
        monsvc::detail::Registry::instance().clear();
    }

    static std::vector<std::string> collect_names() {
        std::vector<std::string> expected_names;
        std::function<void(monsvc::detail::Registry::Entry)> fn =
          std::bind(&MonitoringServiceFixture::collect, std::ref(expected_names), std::placeholders::_1);
        monsvc::detail::Registry::instance().for_each(fn);
        return expected_names;
    }

private:
    static void collect(std::vector<std::string> & names, monsvc::detail::Registry::Entry entry) {
        names.push_back(entry->name());
    }
};

BOOST_FIXTURE_TEST_SUITE(MonitoringServiceTest, MonitoringServiceFixture)

BOOST_AUTO_TEST_CASE(test_add_remove_iterate)
{
    TestInfo info1, info2;
    std::string name1("info1"), name2("info2");
    monsvc::ptr<TestInfo> p;
    
    // Register two objects.
    p = get_service().register_object(name1, &info1);
    BOOST_CHECK_EQUAL(p.get(), &info1);

    p = get_service().register_object(name2, &info2);
    BOOST_CHECK_EQUAL(p.get(), &info2);

    // Search the first one.
    p = get_service().find_object<TestInfo>(name1);
    BOOST_CHECK_EQUAL(p.get(), &info1);

    // Search an inexistent object.
    BOOST_CHECK_THROW(
            get_service().find_object<TestInfo>("some_inexistent_name"),
            monsvc::NotFound);
    // Search an object with the wrong type.
    BOOST_CHECK_THROW(
        get_service().find_object<int>(name1),
        monsvc::WrongType);

    // Iterate the Registry an check the names.
    std::vector<std::string> names = collect_names();
    std::sort(names.begin(), names.end());
    BOOST_REQUIRE_EQUAL(names.size(), 2);
    BOOST_CHECK_EQUAL(names[0], name1);
    BOOST_CHECK_EQUAL(names[1], name2);

    // Remove the first object.
    get_service().remove_object(&info1);

    // We cannot add an object twice.
    BOOST_CHECK_THROW(
            get_service().register_object(name2, &info1),
            monsvc::NameExists);

    // Remove the second object.
    BOOST_CHECK_EQUAL(get_service().remove_object(name2.c_str()), true);
    BOOST_CHECK_EQUAL(get_service().remove_object(name2), false);
}

BOOST_AUTO_TEST_CASE(test_add_remove_iterate_with_tags)
{
    TestInfo info1, info2;
    std::string name1("info1"), name2("info2");
    monsvc::ptr<TestInfo> p;

    // Register two objects with same name, but different tags --> ok
    p = get_service().register_object(name1, &info1);
    BOOST_CHECK_EQUAL(p.get(), &info1);

    p = get_service().register_object(name1, 2, &info2);
    BOOST_CHECK_EQUAL(p.get(), &info2);

    //A an object with same name & tag will fail
    BOOST_CHECK_THROW(
                      get_service().register_object(name1, 2, &info2),
                      monsvc::NameExists);
    
    //Find default object
    p = get_service().find_object<TestInfo>(name1);
    BOOST_CHECK_EQUAL(p.get(), &info1);

    //Find object with tag 2
    p = get_service().find_object<TestInfo>(name1, 2);
    BOOST_CHECK_EQUAL(p.get(), &info2);
    
    //Find unexisting tag
    BOOST_CHECK_THROW(
            get_service().find_object<TestInfo>(name1, 10);,
            monsvc::NotFound);
    
    // Iterate the Registry an check the names.
    std::vector<std::string> names = collect_names();
    BOOST_REQUIRE_EQUAL(names.size(), 2);
    BOOST_CHECK_EQUAL(names[0], name1);
    BOOST_CHECK_EQUAL(names[1], name1);
    
    // Remove the first object.
    BOOST_CHECK_EQUAL(get_service().remove_object(&info1), true);
    BOOST_CHECK_EQUAL(get_service().remove_object(name1, 2), true);
    
    // Remove unexisting tag will fail
    BOOST_CHECK_EQUAL(get_service().remove_object(name1, 3), false);

    p = get_service().register_object(name1, &info1);
    p = get_service().register_object(name1, 2, &info2);
    p = get_service().register_object(name2, &info2);
    
    //Remove all objects with same name
    BOOST_CHECK_EQUAL(get_service().remove_objects(name1), 2);
    
    BOOST_CHECK_EQUAL(get_service().remove_object(name2), true);
}

BOOST_AUTO_TEST_CASE(test_insert_lookup_different_types)
{
    TestInfo info1;
    std::string name1("info1");

    // Insert ISInfo * and lookup for and delete TestInfo *.
    get_service().register_object(name1, static_cast<ISInfo *>(&info1));
    BOOST_CHECK_THROW(
            get_service().find_object<TestInfo>(name1),
            monsvc::WrongType);
    BOOST_CHECK_EQUAL(get_service().remove_object(&info1), false);
    BOOST_CHECK_EQUAL(get_service().remove_object(static_cast<ISInfo *>(&info1)), true);

    // Insert TestInfo* and lookup and delete ISInfo*.
    get_service().register_object(name1, &info1);
    BOOST_CHECK_THROW(
            get_service().find_object<ISInfo>(name1),
            monsvc::WrongType);
    BOOST_CHECK_EQUAL(get_service().remove_object(static_cast<ISInfo*>(&info1)), false);
    BOOST_CHECK_EQUAL(get_service().remove_object(&info1), true);
}

// Class used to check whether we free the objects correctly.
class InstrumentedDestructorInfo : public TestInfo {
public:
    ~InstrumentedDestructorInfo() {
        destructor_called = true;
    }
    static bool destructor_called;
};
bool InstrumentedDestructorInfo::destructor_called = false;


BOOST_AUTO_TEST_CASE(test_ownership)
{
    InstrumentedDestructorInfo::destructor_called = false;
    InstrumentedDestructorInfo * info = new InstrumentedDestructorInfo;
    std::string info_name = "info";

    // The registry does not own the object, so it does not free it.
    get_service().register_object(info_name, info, false);
    get_service().remove_object(info_name);
    BOOST_CHECK_EQUAL(InstrumentedDestructorInfo::destructor_called, false);

    // The registry owns the object and frees it.
    get_service().register_object(info_name, info, true);
    get_service().remove_object(info_name);
    BOOST_CHECK_EQUAL(InstrumentedDestructorInfo::destructor_called, true);
}


class Callback {
public:
    Callback() : called(false) {}
    void operator()(const std::string &, TestInfo * ) {
        called = true;
    }
    static void call(const std::string &, TestInfo *){
        s_called = true;
    }
    void operator()(const std::string &, TGraph2D * ) {
        called = true;
    }
    static void call(const std::string &, TGraph2D *){
        s_called = true;
    }
    bool called;
    static bool s_called;
};

// This class attempts to publish just to
class DummyPublisher : public monsvc::PublisherBase {
public:
    using monsvc::PublisherBase::publish;
    using monsvc::PublisherBase::knows;

    DummyPublisher() : TH1_published(false), TGraph_published(false),
                       TGraph2D_published(false), TEfficiency_published(false),
                       ISInfo_published(false) {}

    bool TH1_published;
    bool TGraph_published;
    bool TGraph2D_published;
    bool TEfficiency_published;
    bool ISInfo_published;

    virtual void 
    publish(const std::string&, const monsvc::Tag&,
            const monsvc::Annotations&, TH1 *) override
    {
        TH1_published = true;
    }
    virtual bool knows(TH1 *) override { return true; }
    
    virtual void 
    publish(const std::string&, const monsvc::Tag&,
            const monsvc::Annotations&, TGraph *) override
    {
        TGraph_published = true;
    }
    virtual bool knows(TGraph *) override { return true; }

    virtual void
    publish(const std::string&, const monsvc::Tag&,
            const monsvc::Annotations&, TEfficiency *) override
    {
        TEfficiency_published = true;
    }
    virtual bool knows(TEfficiency *) override { return true; }

    virtual void 
    publish(const std::string&, const monsvc::Tag&,
            const monsvc::Annotations&, ISInfo *) override
    {
        ISInfo_published = true;
    }
    virtual bool knows(ISInfo *) override { return true; }

    void publish_now(monsvc::detail::Registry::Entry entry) {
        entry->publish(*this);
    }
};

BOOST_AUTO_TEST_CASE(test_callback_std)
{
    DummyPublisher pub;
    Callback cb1;
    std::function<void(const std::string&, TestInfo *)>  callback1 = 
        std::ref(cb1);
    Callback cb2;
    std::function<void(const std::string&, TGraph2D *)>  callback2 = 
        std::ref(cb2);
    Callback cb3;
    std::function<void(const std::string&, TestInfo *)>  callback3 = 
        std::ref(cb3);
    Callback cb4;
    std::function<void(const std::string&, TestInfo *)>  callback4 = 
        std::ref(cb4);
    Callback cb5;
    std::function<void(const std::string&, TestInfo *)>  callback5 = 
        std::ref(cb5);
    
    std::function<void(monsvc::detail::Registry::Entry)> publish_now =
      std::bind(&DummyPublisher::publish_now, &pub, std::placeholders::_1);

    // The registry does not own the object, so it does not free it.
    get_service().register_object("info1", static_cast<TestInfo *>(0), 
                                  false, callback1);
    get_service().register_object("info2", static_cast<TH1 *>(0));
    get_service().register_object("info3", static_cast<TGraph *>(0));

    // Test null callback
    std::function<void(const std::string&, TestInfo *)> c(0);
    get_service().register_object("info4", static_cast<TestInfo *>(0),
                                  false, c);

    // Test call not called if object not published
    get_service().register_object("info5", static_cast<TGraph2D *>(0), 
                                  false, callback2);
    
    std::function<void(const std::string&, TGraph2D *)> b(0);
    get_service().register_object("info6", static_cast<TGraph2D *>(0), 
                                  false, b, callback2);
    get_service().register_object("info7", static_cast<TGraph2D *>(0), 
                                  false, callback2, callback2);

    
    // Test post publish callback
    get_service().register_object("info8", static_cast<TestInfo *>(0),
                                  false, c, callback3);

    // Test pre & post callback
    get_service().register_object("info9", static_cast<TestInfo *>(0),
                                  false, callback4, callback5);
    
    monsvc::detail::Registry::instance().for_each(publish_now);

    // Check that the callback 
    BOOST_CHECK_EQUAL(cb1.called, true);
    BOOST_CHECK_EQUAL(cb3.called, true);
    BOOST_CHECK_EQUAL(cb4.called, true);
    BOOST_CHECK_EQUAL(cb5.called, true);        

    // The callback should not have been called since the publisher does not
    // support TGraph2D
    BOOST_CHECK_EQUAL(cb2.called, false);

    // Check the publish methods were executed.
    BOOST_CHECK_EQUAL(pub.ISInfo_published, true);
    BOOST_CHECK_EQUAL(pub.TH1_published, true);
    BOOST_CHECK_EQUAL(pub.TGraph_published, true);
}

BOOST_AUTO_TEST_CASE(test_callback_boost)
{
    DummyPublisher pub;
    Callback cb;
    boost::function<void(const std::string&, TestInfo *)>  callback = 
      boost::ref(cb);
    
    // The registry does not own the object, so it does not free it.
    get_service().register_object("info_boost", static_cast<TestInfo *>(0), false, callback);

    boost::function<void(monsvc::detail::Registry::Entry)> publish_now =
        boost::bind(&DummyPublisher::publish_now, &pub, boost::placeholders::_1);

    monsvc::detail::Registry::instance().for_each(publish_now);

    BOOST_CHECK_EQUAL(cb.called, true);
}

Callback cb_functor;
bool Callback::s_called = false;

void cb_wrapper(const std::string &a, TestInfo *b){
  cb_functor.call(a,b);
}

BOOST_AUTO_TEST_CASE(test_callback_functor)
{
    DummyPublisher pub;
        
    // The registry does not own the object, so it does not free it.
    get_service().register_object("info_functor", static_cast<TestInfo *>(0), false, cb_wrapper);
    
    std::function<void(monsvc::detail::Registry::Entry)> publish_now =
      std::bind(&DummyPublisher::publish_now, &pub, std::placeholders::_1);

    monsvc::detail::Registry::instance().for_each(publish_now);

    BOOST_CHECK_EQUAL(Callback::s_called, true);
}

BOOST_AUTO_TEST_CASE(test_callback_mix)
{
    DummyPublisher pub;
    Callback::s_called = false;

    Callback cb1;
    std::function<void(const std::string&, TestInfo *)>  callback1 = 
        std::ref(cb1);
    Callback cb2;
    boost::function<void(const std::string&, TestInfo *)>  callback2 = 
      boost::ref(cb2);
    Callback cb3;
    std::function<void(const std::string&, TestInfo *)>  callback3 = 
        std::ref(cb3);
    Callback cb4;
    boost::function<void(const std::string&, TestInfo *)>  callback4 = 
      boost::ref(cb4);
    
    get_service().register_object("info1", static_cast<TestInfo *>(0), false, callback1, callback2);
    get_service().register_object("info2", static_cast<TestInfo *>(0), false, callback3, cb_wrapper);

    std::function<void(monsvc::detail::Registry::Entry)> publish_now =
        std::bind(&DummyPublisher::publish_now, &pub, std::placeholders::_1);

    monsvc::detail::Registry::instance().for_each(publish_now);

    BOOST_CHECK_EQUAL(cb1.called, true);
    BOOST_CHECK_EQUAL(cb2.called, true);
    BOOST_CHECK_EQUAL(cb3.called, true);
    BOOST_CHECK_EQUAL(Callback::s_called, true);

    Callback::s_called = false;
    
    get_service().register_object("info3", static_cast<TestInfo *>(0), false, cb_wrapper, callback4);

    monsvc::detail::Registry::instance().for_each(publish_now);

    BOOST_CHECK_EQUAL(cb4.called, true);
    BOOST_CHECK_EQUAL(Callback::s_called, true);
}

class LockAwareInfo : public TestInfo {
public:
    bool was_locked(monsvc::ptr<LockAwareInfo> p) {
        bool locked = p.try_lock();
        if (locked) {
            p.unlock();
        }
        return !locked;
    }
};

BOOST_AUTO_TEST_CASE(test_lock)
{
    LockAwareInfo info;
    monsvc::ptr<LockAwareInfo> p = get_service().register_object("info", &info);

    // If we call the method using the smart pointer, the lock is held.
    BOOST_CHECK_EQUAL(p->was_locked(p), true);

    // If we use the raw pointer, the lock is not held.
    BOOST_CHECK_EQUAL(p.get()->was_locked(p), false);

    {
        // We acquire the lock explicitly.
        monsvc::ptr<LockAwareInfo>::scoped_lock lock(p);
        BOOST_CHECK_EQUAL(p.get()->was_locked(p), true);
    }
}

BOOST_AUTO_TEST_SUITE_END()

/* Local Variables: */
/* c-basic-offset:4 */
/* End: */
