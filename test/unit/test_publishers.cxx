#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE monsvc Publishers Tests
#define BOOST_TEST_MAIN

#include <boost/test/unit_test.hpp>

#include <memory>
#include <boost/algorithm/string.hpp>

#include <is/info.h>
#include <is/infoiterator.h>
#include <is/criteria.h>

#include <oh/OHRootReceiver.h>

#include <iostream>
#include <cstdio>

#include <TFile.h>

#include "monsvc/PublishingController.h"
#include "monsvc/MonitoringService.h"
#include "monsvc/ConfigurationRules.h"
#include "PeriodicScheduler.h"
#include "ISPublisher.h"
#include "OHPublisher.h"
#include "OstreamPublisher.h"
#include "FilePublisher.h"
#include "monsvc/Exceptions.h"
#include "monsvc/NameFilter.h"

#include "Registry.h"

#include "test/unit/test_online_fixture.h"

/**
 * Tests the implementations of the PublisherBase.
 */

class PublisherTestFixture : public OnlineTestFixture {
public:
    ~PublisherTestFixture() {
        monsvc::detail::Registry::instance().clear();
    }
};

BOOST_FIXTURE_TEST_SUITE(PublishersTest, PublisherTestFixture)

class TestInfo : public ISInfo {
public:
    TestInfo(int x) : ISInfo("TestInfo"), m_x(x) {}
    void publishGuts(ISostream& s) { s << m_x; }
    void refreshGuts(ISistream& s) { s >> m_x; }
    std::ostream& print(std::ostream& s) const { s << "x: " << m_x << std::endl ; return s; }
    int m_x;
};

BOOST_AUTO_TEST_CASE(check_is_publisher)
{
    const std::string object_name = "is_object_name";

    // Create the publisher by using a rule.
    std::shared_ptr<monsvc::ConfigurationRule> rule =
            monsvc::ConfigurationRule::from("rule:.*/=>is:(2,1," + get_server_name() + ")");

    // Check that the publisher has the right type.
    std::shared_ptr<monsvc::PublisherBase> pub =
            rule->create_publisher(get_partition(), get_app_name());
    std::shared_ptr<monsvc::ISPublisher> ispub =
            std::dynamic_pointer_cast<monsvc::ISPublisher>(pub);
    BOOST_REQUIRE(ispub);

    // Publish an object.
    const int EXPECTED_X = 2;
    TestInfo info(EXPECTED_X);
    monsvc::Tag nulltag;
    monsvc::Annotations ann;
    pub->publish(object_name, nulltag, ann, &info);

    ISInfoDictionary dict(get_partition());
    TestInfo retrieved(EXPECTED_X + 1);
    dict.findValue(get_server_name() + "." 
                   + get_app_name() + "." 
                   + object_name,retrieved);

    BOOST_CHECK_EQUAL(retrieved.m_x, EXPECTED_X);


    std::vector<std::pair<int,OWLTime>> tags;
    dict.getTags(get_server_name() + "." 
                 + get_app_name() + "." 
                 + object_name, tags);

    //The default tag in IS is '0'
    BOOST_CHECK_EQUAL(tags.size(), 1);
    BOOST_CHECK_EQUAL(tags[0].first, 0);

    info.m_x = EXPECTED_X+1;
    pub->publish(object_name, monsvc::Tag(1), ann, &info);
    info.m_x = EXPECTED_X+2;
    pub->publish(object_name, monsvc::Tag(2), ann, &info);

    dict.getTags(get_server_name() + "." 
                 + get_app_name() + "." 
                 + object_name, tags);
    
    BOOST_CHECK_EQUAL(tags.size(), 3);
    std::sort(tags.begin(), tags.end(), [](const std::pair<int,OWLTime>& i, 
                                           const std::pair<int,OWLTime>& o)
              { return i.first < o.first; });
    BOOST_CHECK_EQUAL(tags[0].first, 0);
    BOOST_CHECK_EQUAL(tags[1].first, 1);
    BOOST_CHECK_EQUAL(tags[2].first, 2);
}

BOOST_AUTO_TEST_CASE(check_oh_publisher)
{
    const std::string provider_name = "provider_name";

    // Create the publisher by using a rule.
    std::shared_ptr<monsvc::ConfigurationRule> rule = 
      monsvc::ConfigurationRule::from(
                                      "rule:.*/=>oh:(2,1," + get_server_name() +
                                      "," + provider_name + ")");

    // Check that the publisher has the right type.
    std::shared_ptr<monsvc::PublisherBase> pub =
            rule->create_publisher(get_partition(), get_app_name());
    std::shared_ptr<monsvc::OHPublisher> ohpub =
            std::dynamic_pointer_cast<monsvc::OHPublisher>(pub);
    BOOST_REQUIRE(ohpub);

    // Publish a histogram.
    const std::string hname("histo");
    TH1F histo(hname.c_str(), hname.c_str(), 100, 0., 100.);
    histo.Fill(50.);
    monsvc::Tag nulltag;
    monsvc::Annotations ann;
    ohpub->publish(hname, nulltag, ann, &histo);

    // And check that we can retrieve it from the OH server.
    OHRootHistogram recvd_histo = OHRootReceiver::getRootHistogram(
            get_partition(), get_server_name(), provider_name, hname);
    BOOST_CHECK_EQUAL(recvd_histo.getObject()->GetMaximumBin(), 51);

    // Check tag
    BOOST_CHECK_EQUAL(recvd_histo.tag, -1);

    // Tag = -1 is a special value in OH
    // it will become "unreachable" once Tag != -1 are
    // published
    
    histo.Fill(51);
    histo.Fill(51);
    ohpub->publish(hname, monsvc::Tag(0), ann, &histo);

    recvd_histo = 
      OHRootReceiver::getRootHistogram(
                                       get_partition(), 
                                       get_server_name(), 
                                       provider_name, hname, 0);
    BOOST_CHECK_EQUAL(recvd_histo.tag, 0);
    BOOST_CHECK_EQUAL(recvd_histo.getObject()->GetMaximumBin(), 52);

    histo.Fill(52);
    histo.Fill(52);
    histo.Fill(52);
    ohpub->publish(hname, monsvc::Tag(1), ann, &histo);
    // Tag 0 should be unchanged
    recvd_histo = 
      OHRootReceiver::getRootHistogram(
                                       get_partition(), 
                                       get_server_name(), 
                                       provider_name, hname, 0);
    BOOST_CHECK_EQUAL(recvd_histo.tag, 0);
    BOOST_CHECK_EQUAL(recvd_histo.getObject()->GetMaximumBin(), 52);

    recvd_histo = OHRootReceiver::getRootHistogram(get_partition(), 
                                                   get_server_name(), 
                                                   provider_name, hname, 1);
    BOOST_CHECK_EQUAL(recvd_histo.tag, 1);
    BOOST_CHECK_EQUAL(recvd_histo.getObject()->GetMaximumBin(), 53);
    
    // Publish a histogram as a TObject
    const std::string hname1("histo1");
    TH1F histo1(hname1.c_str(), hname1.c_str(), 100, 0., 100.);
    histo1.Fill(50.);
    ohpub->publish(hname1, nulltag, ann, dynamic_cast<TObject*>(&histo1));

    // And check that we can retrieve it from the OH server.
    recvd_histo = OHRootReceiver::getRootHistogram(
        get_partition(), get_server_name(), provider_name, hname1);
    BOOST_CHECK_EQUAL(recvd_histo.getObject()->GetMaximumBin(), 51);
}

BOOST_AUTO_TEST_CASE(check_dual_oh_publisher)
{
    const std::string provider_name = "dual_provider_name";
    const std::string object_name = "oh_object_name";

    // Create the publishers by using a rules.
    std::shared_ptr<monsvc::ConfigurationRule> rule1 = 
      monsvc::ConfigurationRule::from(
                                      "rule1:.*/=>oh:(2,1," + 
                                      get_server_name() + "," + 
                                      provider_name +")");
    std::shared_ptr<monsvc::PublisherBase> pub1 =
            rule1->create_publisher(get_partition(), get_app_name());
    std::shared_ptr<monsvc::ConfigurationRule> rule2 = 
      monsvc::ConfigurationRule::from(
                                      "rule2:.*/=>oh:(2,1," + 
                                      get_server_name() + "," + 
                                      provider_name + ")");
    std::shared_ptr<monsvc::PublisherBase> pub2 =
            rule2->create_publisher(get_partition(), get_app_name());
    
    // Publish the same histogram in both provider instances
    TH1F histo("histo","histo", 100, -0.5, 99.5);
    histo.Fill(60.);
    monsvc::Tag nulltag;
    monsvc::Annotations ann;
    pub1->publish(object_name, nulltag, ann, &histo);
    pub2->publish(object_name, nulltag, ann, &histo);

    // And check that we can retrieve it from the OH server.
    OHRootHistogram recvd_histo = OHRootReceiver::getRootHistogram(
            get_partition(), get_server_name(), provider_name, object_name);
    BOOST_CHECK_EQUAL(recvd_histo.getObject()->GetMaximumBin(), 61);

    
    // Publish different histograms in the provider instances
    std::string hname1("histo1");
    std::string hname2("histo2");
    TH1F histo1(hname1.c_str(),hname1.c_str(), 100, -0.5, 99.5);
    histo1.Fill(1.);
    pub1->publish(hname1, nulltag, ann, &histo1);
    TH1F histo2(hname2.c_str(),hname2.c_str(), 100, -0.5, 99.5);
    histo2.Fill(2.);
    pub2->publish(hname2, nulltag, ann, &histo2);
    
    OHRootHistogram recvd_histo1 = OHRootReceiver::getRootHistogram(
            get_partition(), get_server_name(), provider_name, hname1);
    BOOST_CHECK_EQUAL(recvd_histo1.getObject()->GetMaximumBin(), 2);
    OHRootHistogram recvd_histo2 = OHRootReceiver::getRootHistogram(
            get_partition(), get_server_name(), provider_name, hname2);
    BOOST_CHECK_EQUAL(recvd_histo2.getObject()->GetMaximumBin(), 3);
}

BOOST_AUTO_TEST_CASE(check_ostream_publisher)
{
    // Publish a TestInfo object in a string.
    std::stringstream out;
    std::shared_ptr<monsvc::OstreamPublisher> pub(
            std::make_shared<monsvc::OstreamPublisher>(std::ref(out)));

    TestInfo info1(1);
    monsvc::Tag nulltag;
    monsvc::Annotations ann;
    pub->publish("info1", nulltag, ann, &info1);

    // Check that we published the object.
    BOOST_CHECK(boost::contains(out.str(), "x: 1"));

    pub->publish("info1", monsvc::Tag(2), ann, &info1);

    // Check tags
    BOOST_CHECK(boost::contains(out.str(), "Tag: None"));
    BOOST_CHECK(boost::contains(out.str(), "Tag: 2"));

}

BOOST_AUTO_TEST_CASE(check_file_publisher)
{
    TH1F histo("histo","histo", 100, 0., 100.);
    histo.Fill(70.);

    // Publish the histogram.
    TFile fout("out.root", "RECREATE");
    std::shared_ptr<monsvc::FilePublisher> pub(
            std::make_shared<monsvc::FilePublisher>(&fout));
    monsvc::Tag nulltag;
    monsvc::Annotations ann;
    pub->publish("histo", nulltag, ann, &histo);
    fout.Write();
    fout.Close();

    // And check that we can retrieve it from the ROOT file.
    TFile fin("out.root");
    TH1F * recvd_histo = 0;
    BOOST_CHECK_NO_THROW(
            recvd_histo = static_cast<TH1F *>(fin.GetObjectUnchecked("histo")));
    BOOST_CHECK_EQUAL(recvd_histo->GetMaximumBin(), 71);
    fin.Close();

    ::remove("out.root");
}

class GrumpyPublisher : public monsvc::PublisherBase {
    using monsvc::PublisherBase::publish;
    virtual void publish(const std::string& name, const monsvc::Tag&,
                         const monsvc::Annotations&, ISInfo *) override {
        throw monsvc::ISPublishingIssue(ERS_HERE, name);
    }
    virtual void publish(const std::string&, const monsvc::Tag&, 
                         const monsvc::Annotations&, TH1 *) override{
        throw std::bad_alloc();
    }
};

BOOST_AUTO_TEST_CASE(check_publisher_exceptions)
{
    monsvc::MonitoringService::instance().register_object("info1", static_cast<ISInfo *>(0));
    monsvc::MonitoringService::instance().register_object("th1", static_cast<TH1 *>(0));

    // The publishing related exceptions are logged and masked.
    std::shared_ptr<GrumpyPublisher> pub(std::make_shared<GrumpyPublisher>());
    monsvc::NameFilter info_filter("info1");
    monsvc::detail::PeriodicScheduler::PeriodicTask task1(pub, info_filter);
    BOOST_CHECK_NO_THROW(task1.publish(0, 1));

    // Other exceptions are thrown.
    monsvc::NameFilter histo_filter("th1");
    monsvc::detail::PeriodicScheduler::PeriodicTask task2(pub, histo_filter);
    BOOST_CHECK_THROW(task2.publish(0, 1), std::bad_alloc);
}

class CollectingPublisher : public monsvc::PublisherBase {
public:
    CollectingPublisher(std::set<std::string>& published_names)
        : m_published_names(published_names) {}
    std::set<std::string>& m_published_names;

    using monsvc::PublisherBase::publish;
    virtual void publish(const std::string& name, const monsvc::Tag&, 
                         const monsvc::Annotations&, ISInfo *) override
    {
        m_published_names.insert(name);
    }
    virtual void publish(const std::string& name, const monsvc::Tag&, 
                         const monsvc::Annotations&, TH1 *) override
    {
        m_published_names.insert(name);
    }
};

BOOST_AUTO_TEST_CASE(check_publish_all)
{
    const std::string provider_name = "provider_name";

    // Create the publisher by using a rule.
    std::shared_ptr<monsvc::ConfigurationRule> rule = 
      monsvc::ConfigurationRule::from(
                                      "rule:.*/=>oh:(1000,1," + get_server_name() +
                                      "," + provider_name + ")");
    
    monsvc::PublishingController pub(get_partition(), get_app_name());
    pub.add_configuration_rule(*rule);
    
    // Publish a histogram.
    const std::string hname("histo");
    TH1F histo(hname.c_str(), hname.c_str(), 100, 0., 100.);
    const std::string hname1("histo1");
    TH1F histo1(hname1.c_str(), hname1.c_str(), 100, 0., 100.);
    histo.Fill(80.);
    monsvc::MonitoringService::instance().register_object(hname, &histo);
    
    pub.publish_all();

    OHRootHistogram recvd_histo = OHRootReceiver::getRootHistogram(
            get_partition(), get_server_name(), provider_name, hname);
    BOOST_CHECK_EQUAL(recvd_histo.getObject()->GetName(), 
                      get_server_name()+"."+provider_name+"."+hname);
    BOOST_CHECK_EQUAL(recvd_histo.getObject()->GetMaximumBin(), 81);
    BOOST_CHECK_EQUAL(recvd_histo.tag, -1);

    monsvc::MonitoringService::instance().remove_object(&histo);

    // Tag = -1 is a special value in OH
    // it will become "unreachable" once Tag != -1 are
    // published

    monsvc::MonitoringService::instance().register_object(hname, 0, &histo);

    histo1.Fill(81.);
    monsvc::MonitoringService::instance().register_object(hname, 1, &histo1);
    
    pub.publish_all();
    
    recvd_histo = 
      OHRootReceiver::getRootHistogram(
                                       get_partition(), 
                                       get_server_name(), 
                                       provider_name, hname, 0);
    BOOST_CHECK_EQUAL(recvd_histo.getObject()->GetName(), 
                      get_server_name()+"."+provider_name+"."+hname);
    BOOST_CHECK_EQUAL(recvd_histo.getObject()->GetMaximumBin(), 81);
    BOOST_CHECK_EQUAL(recvd_histo.tag, 0);

    recvd_histo = 
      OHRootReceiver::getRootHistogram(
                                       get_partition(), 
                                       get_server_name(), 
                                       provider_name, hname, 1);
    BOOST_CHECK_EQUAL(recvd_histo.getObject()->GetName(), 
                      get_server_name()+"."+provider_name+"."+hname);
    BOOST_CHECK_EQUAL(recvd_histo.getObject()->GetMaximumBin(), 82);
    BOOST_CHECK_EQUAL(recvd_histo.tag, 1);

}

BOOST_AUTO_TEST_CASE(check_name_filter)
{
    monsvc::MonitoringService::instance().register_object("info1", static_cast<ISInfo *>(0));
    monsvc::MonitoringService::instance().register_object("info2", static_cast<TH1 *>(0));
    monsvc::MonitoringService::instance().register_object("data1", static_cast<ISInfo *>(0));
    monsvc::MonitoringService::instance().register_object("data2", static_cast<ISInfo *>(0));

    std::set<std::string> published_names;
    std::shared_ptr<CollectingPublisher> pub(
            std::make_shared<CollectingPublisher>(std::ref(published_names)));

    // Filter applies regardless the datatype.
    monsvc::NameFilter filter("info.*");
    monsvc::detail::PeriodicScheduler::PeriodicTask task1(pub, filter);
    task1.publish(0, 1);
    BOOST_CHECK_EQUAL(published_names.count("info1"), 1);
    BOOST_CHECK_EQUAL(published_names.count("info2"), 1);
    BOOST_CHECK_EQUAL(published_names.size(), 2);
    published_names.clear();

    // Exclude filter test.
    filter = monsvc::NameFilter(".*", "info.*");
    monsvc::detail::PeriodicScheduler::PeriodicTask task2(pub, filter);
    task2.publish(0, 1);
    BOOST_CHECK_EQUAL(published_names.count("data1"), 1);
    BOOST_CHECK_EQUAL(published_names.count("data2"), 1);
    BOOST_CHECK_EQUAL(published_names.size(), 2);
    published_names.clear();
}

BOOST_AUTO_TEST_CASE(check_slot_filter)
{
    monsvc::MonitoringService::instance().register_object("info1", static_cast<ISInfo *>(0));
    monsvc::MonitoringService::instance().register_object("info2", static_cast<TH1 *>(0));
    monsvc::MonitoringService::instance().register_object("data1", static_cast<ISInfo *>(0));
    monsvc::MonitoringService::instance().register_object("data2", static_cast<ISInfo *>(0));

    std::set<std::string> published_names;
    std::shared_ptr<CollectingPublisher> pub(
                                             std::make_shared<CollectingPublisher>(std::ref(published_names)));

    monsvc::NameFilter filter(".*");
    std::hash<std::string> hash_fn;
    
    unsigned slots = 2;
    unsigned idx = 0;
    monsvc::detail::PeriodicScheduler::PeriodicTask task(pub, filter);

    task.publish(idx, slots);

    BOOST_CHECK_EQUAL(published_names.count("data1"),
                      hash_fn("data1")%slots == idx);
    BOOST_CHECK_EQUAL(published_names.count("data2"),
                      hash_fn("data2")%slots == idx);
    BOOST_CHECK_EQUAL(published_names.count("info1"),
                      hash_fn("info1")%slots == idx);
    BOOST_CHECK_EQUAL(published_names.count("info2"),
                      hash_fn("info2")%slots == idx);

    
    published_names.clear();
    idx = 1;
    task.publish(idx, slots);

    BOOST_CHECK_EQUAL(published_names.count("data1"),
                      hash_fn("data1")%slots == idx);
    BOOST_CHECK_EQUAL(published_names.count("data2"),
                      hash_fn("data2")%slots == idx);
    BOOST_CHECK_EQUAL(published_names.count("info1"),
                      hash_fn("info1")%slots == idx);
    BOOST_CHECK_EQUAL(published_names.count("info2"),
                      hash_fn("info2")%slots == idx);

}

BOOST_AUTO_TEST_SUITE_END()
