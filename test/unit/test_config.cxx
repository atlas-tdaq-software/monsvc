#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE monsvc Configuration Tests
#define BOOST_TEST_MAIN

#include <boost/test/unit_test.hpp>

#include <memory>
#include <config/Configuration.h>
#include <vector>
#include <string>

#include "test/unit/test_online_fixture.h"

#include "monsvc/PublishingController.h"
#include "monsvc/MonitoringService.h"
#include "monsvc/PublisherBase.h"
#include "FilePublisher.h"
#include "ISPublisher.h"

#include <boost/property_tree/ptree.hpp>


/**
 * Test suite for the publishing configuration.
 */

class ConfigTestFixture : public OnlineTestFixture {
public:
    ConfigTestFixture() :
        oc(new monsvc::PublishingController(get_partition(), get_app_name()))
    {
    }

    std::unique_ptr<Configuration> configuration()
    try {
        const char* monsvc_src = getenv("MONSVC_SRC");
        BOOST_TEST_REQUIRE(monsvc_src != nullptr);
        std::string url = "oksconfig:";
        url += monsvc_src;
        url += "/test/unit/monsvc_config.data.xml";
        return std::make_unique<Configuration>(url);
    } catch (const ers::Issue& ex) {
        ers::error(ex);
        BOOST_FAIL(ex.what());
        throw;
    } catch (const std::exception& ex) {
        BOOST_FAIL(ex.what());
        throw;
    }

    std::shared_ptr<monsvc::PublishingController> oc;
};

BOOST_FIXTURE_TEST_SUITE(ConfigTest, ConfigTestFixture)

BOOST_AUTO_TEST_CASE(test_oks_config_by_app)
{
    auto conf = configuration();
    const monsvc::dal::PublishingApplication* app = 
      conf->get<monsvc::dal::PublishingApplication>(get_app_name());
    BOOST_CHECK_NO_THROW(oc->add_configuration_rules(*conf, app));

    // There are two rules in the database, only one for the online configuration.
    std::vector<std::string> rules = oc->get_rules();
    BOOST_REQUIRE_EQUAL(rules.size(), 1);
    BOOST_CHECK_EQUAL(rules[0], "NetworkTraffic:NetworkTraffic/.*/=>is:(10,2,1.2.3.4)");
}

BOOST_AUTO_TEST_CASE(test_oks_config_by_name)
{
    auto conf = configuration();
    BOOST_CHECK_NO_THROW(oc->add_configuration_rules(*conf));

    // There are two rules in the database, only one for the online configuration.
    std::vector<std::string> rules = oc->get_rules();
    BOOST_REQUIRE_EQUAL(rules.size(), 1);
    BOOST_CHECK_EQUAL(rules[0], "NetworkTraffic:NetworkTraffic/.*/=>is:(10,2,1.2.3.4)");
}

BOOST_AUTO_TEST_CASE(test_oks_config_by_non_existing_name)
{
    monsvc::PublishingController msvc(get_partition(), "fake");
    auto conf = configuration();
    BOOST_CHECK_THROW(msvc.add_configuration_rules(*conf),
                      monsvc::ApplicationNotFound);
}

class FakeRule : public monsvc::ConfigurationRule {
public:
    FakeRule(const std::string& name, const std::string& repr,
             unsigned interval, unsigned nslots) :
        ConfigurationRule(name, monsvc::NameFilter(), interval, nslots), repr(repr) {
    }

    const std::string params_str() const
    {
        return repr;
    }

    std::shared_ptr<monsvc::PublisherBase> create_publisher(
            const IPCPartition&, const std::string&) const
    {
        return std::shared_ptr<monsvc::PublisherBase>();
    }

    const std::string repr;
};

BOOST_AUTO_TEST_CASE(test_override_config_static)
{
    std::vector<std::string> rules;

    // A rule with interval 0 is ignored.
    BOOST_CHECK_NO_THROW(
                         oc->add_configuration_rule(FakeRule("existing_rule", "type:(params1)", 0, 1))
    );
    rules = oc->get_rules();
    BOOST_CHECK_EQUAL(rules.size(), 0);

    // Add a rule.
    oc->add_configuration_rule(FakeRule("existing_rule", "type:(params1)", 1, 1));
    rules = oc->get_rules();
    BOOST_REQUIRE_EQUAL(rules.size(), 1);
    BOOST_CHECK_EQUAL(rules[0], "existing_rule:.*/=>type:(params1)");

    // Override it.
    oc->add_configuration_rule(FakeRule("existing_rule", "type:(params2)", 1, 1));
    rules = oc->get_rules();
    BOOST_REQUIRE_EQUAL(rules.size(), 1);
    BOOST_CHECK_EQUAL(rules[0], "existing_rule:.*/=>type:(params2)");

    // Add another rule.
    oc->add_configuration_rule(FakeRule("new_rule", "type:(params3)", 1, 1));
    rules = oc->get_rules();
    BOOST_REQUIRE_EQUAL(rules.size(), 2);
    sort(rules.begin(), rules.end());
    BOOST_CHECK_EQUAL(rules[0], "existing_rule:.*/=>type:(params2)");
    BOOST_CHECK_EQUAL(rules[1], "new_rule:.*/=>type:(params3)");

    // Delete the initial rule.
    oc->disable_configuration_rule("existing_rule");
    rules = oc->get_rules();
    BOOST_REQUIRE_EQUAL(rules.size(), 1);
    BOOST_CHECK_EQUAL(rules[0], "new_rule:.*/=>type:(params3)");

    // Try to delete a rule that does not exist. Nothing should happen.
    oc->disable_configuration_rule("invalid_rule_name");
    rules = oc->get_rules();
    BOOST_REQUIRE_EQUAL(rules.size(), 1);
    BOOST_CHECK_EQUAL(rules[0], "new_rule:.*/=>type:(params3)");
}

BOOST_AUTO_TEST_CASE(test_two_rules_same_provider)
{
    std::shared_ptr<monsvc::ConfigurationRule> rule;
    rule = monsvc::ConfigurationRule::from("rule1:.*/=>oh:(5,1,DF,provider_name)");
    oc->add_configuration_rule(*rule);
    
    rule = monsvc::ConfigurationRule::from("rule2:.*/=>oh:(5,10,DF,provider_name)");
    BOOST_CHECK_NO_THROW(oc->add_configuration_rule(*rule));
}


BOOST_AUTO_TEST_CASE(test_config_rules)
{
    std::shared_ptr<monsvc::ConfigurationRule> rule;

    // OH rule.
    rule = monsvc::ConfigurationRule::from("new_rule:.*/=>oh:(5,1,DF,provider_name)");
    BOOST_CHECK_EQUAL(rule->str(), "new_rule:.*/=>oh:(5,1,DF,provider_name)");

    // IS rule.
    rule = monsvc::ConfigurationRule::from("new_rule:.*/EXPERT.*=>is:(5,2,DF)");
    BOOST_CHECK_EQUAL(rule->str(), "new_rule:.*/EXPERT.*=>is:(5,2,DF)");

    // Server from env
    rule = monsvc::ConfigurationRule::from("new_rule:.*/EXPERT.*=>is:(5,1,${TDAQ_IS_SERVER_NAME})");
    BOOST_CHECK_EQUAL(rule->str(), "new_rule:.*/EXPERT.*=>is:(5,1,DF)");

    // Server from default
    rule = monsvc::ConfigurationRule::from("new_rule:.*/EXPERT.*=>oh:(5,1,${TDAQ_IS_SERVER_NAME_WRONG=Default},provider)");
    BOOST_CHECK_EQUAL(rule->str(), "new_rule:.*/EXPERT.*=>oh:(5,1,Default,provider)");

    // Provider from env
    rule = monsvc::ConfigurationRule::from("new_rule:.*/EXPERT.*=>oh:(5,1,server,${TDAQ_IS_SERVER_NAME})");
    BOOST_CHECK_EQUAL(rule->str(), "new_rule:.*/EXPERT.*=>oh:(5,1,server,DF)");

    
    // Provider from env with suffix
    rule = monsvc::ConfigurationRule::from("new_rule:.*/EXPERT.*=>oh:(5,1,server,${TDAQ_IS_SERVER_NAME}_XXX)");
    BOOST_CHECK_EQUAL(rule->str(), "new_rule:.*/EXPERT.*=>oh:(5,1,server,DF_XXX)");

    // The interval is not a number => exception.
    BOOST_CHECK_THROW(
            rule = monsvc::ConfigurationRule::from("new_rule:.*/EXPERT.*=>is:(five,1,DF)"),
            boost::bad_lexical_cast);

    // The nslots is not a number => exception.
    BOOST_CHECK_THROW(
            rule = monsvc::ConfigurationRule::from("new_rule:.*/EXPERT.*=>is:(5,one,DF)"),
            boost::bad_lexical_cast);

    // The rule type is not IS or OH => bad format.
    BOOST_CHECK_THROW(
            rule = monsvc::ConfigurationRule::from("new_rule:.*/=>XX:(5,1,DF,provider_name)"),
            monsvc::BadRuleFormat);
    
    // The server variable does not exists and the default is not set
    BOOST_CHECK_THROW(
                      rule = monsvc::ConfigurationRule::from("new_rule:.*/EXPERT.*=>is:(5,1,${DOES_NOT_EXIST})"),
                      monsvc::BadRuleFormat);

    BOOST_CHECK_THROW(
                      rule = monsvc::ConfigurationRule::from("new_rule:.*/EXPERT.*=>is:(5,1,${DOES_NOT_EXIST}_XXX)"),
                      monsvc::BadRuleFormat);
}

BOOST_AUTO_TEST_CASE(test_config_rules_ptree)
{
  std::shared_ptr<monsvc::ConfigurationRule> rule;
  
  //OH Rule
  boost::property_tree::ptree ohrule;
  ohrule.add("UID", "OHRule");
  ohrule.add("IncludeFilter", ".*");
  ohrule.add("ExcludeFilter", "");
  ohrule.add("Name", "");
  ohrule.add("Parameters.OHPublishingParameters.UID", "OHParams");
  ohrule.add("Parameters.OHPublishingParameters.PublishInterval", 10);
  ohrule.add("Parameters.OHPublishingParameters.NumberOfSlots", 5);
  ohrule.add("Parameters.OHPublishingParameters.OHServer", "Histogramming");
  ohrule.add("Parameters.OHPublishingParameters.ROOTProvider", "provider");

  rule = monsvc::ConfigurationRule::from(ohrule);
  
  BOOST_CHECK_EQUAL(rule->str(), "OHRule:.*/=>oh:(10,5,Histogramming,provider)");

  //IS Rule
  boost::property_tree::ptree isrule;
  isrule.add("UID", "ISRule");
  isrule.add("IncludeFilter", ".*");
  isrule.add("ExcludeFilter", ".*");
  isrule.add("Name", "Myname");

  //Rule with missing parameters
  BOOST_CHECK_THROW(monsvc::ConfigurationRule::from(isrule),
                    monsvc::BadRuleType);

  isrule.add("Parameters.ISPublishingParameters.UID", "ISParams");
  isrule.add("Parameters.ISPublishingParameters.PublishInterval", 2);
  isrule.add("Parameters.ISPublishingParameters.NumberOfSlots", 1);
  isrule.add("Parameters.ISPublishingParameters.ISServer", "DF");

  rule = monsvc::ConfigurationRule::from(isrule);

  BOOST_CHECK_EQUAL(rule->str(), "Myname:.*/.*=>is:(2,1,DF)");

  //Malformed rule
  boost::property_tree::ptree badrule;
  badrule.add("UID", "ISRule");
  badrule.add("IncludeFilter", 2);
  
  BOOST_CHECK_THROW(monsvc::ConfigurationRule::from(badrule),
                    monsvc::BadRuleFormat);
}

BOOST_AUTO_TEST_CASE(test_config_ptree_nobundles)
{
  boost::property_tree::ptree ohrule;
  ohrule.add("UID", "OHRule");
  ohrule.add("IncludeFilter", ".*");
  ohrule.add("ExcludeFilter", "");
  ohrule.add("Name", "");
  ohrule.add("Parameters.OHPublishingParameters.UID", "OHParams");
  ohrule.add("Parameters.OHPublishingParameters.PublishInterval", 10);
  ohrule.add("Parameters.OHPublishingParameters.NumberOfSlots", 10);
  ohrule.add("Parameters.OHPublishingParameters.OHServer", "DF");
  ohrule.add("Parameters.OHPublishingParameters.ROOTProvider", "provider");

  boost::property_tree::ptree isrule;
  isrule.add("UID", "ISRule");
  isrule.add("IncludeFilter", ".*");
  isrule.add("ExcludeFilter", ".*");
  isrule.add("Name", "Myname");
  isrule.add("Parameters.ISPublishingParameters.UID", "ISParams");
  isrule.add("Parameters.ISPublishingParameters.PublishInterval", 2);
  isrule.add("Parameters.ISPublishingParameters.NumberOfSlots", 2);
  isrule.add("Parameters.ISPublishingParameters.ISServer", "DF");
  
  boost::property_tree::ptree rulebundle;
  rulebundle.add_child("Rules.ConfigurationRule", ohrule);
  rulebundle.add_child("Rules.ConfigurationRule", isrule);

  monsvc::PublishingController msvc(get_partition(), "fake");
  msvc.add_configuration_rules(rulebundle);
    
  boost::property_tree::ptree testrule;
  testrule.add("UID", "TestRule");
  testrule.add("IncludeFilter", ".*");
  testrule.add("ExcludeFilter", "");
  testrule.add("Name", "");
  testrule.add("Parameters.OHPublishingParameters.UID", "OHParams");
  testrule.add("Parameters.OHPublishingParameters.PublishInterval", 10);
  testrule.add("Parameters.OHPublishingParameters.NumberOfSlots", 1);
  testrule.add("Parameters.OHPublishingParameters.OHServer", "DF");
  testrule.add("Parameters.OHPublishingParameters.ROOTProvider", "provider");

  msvc.add_configuration_rule(testrule);

  std::vector<std::string> rules = msvc.get_rules();
  BOOST_REQUIRE_EQUAL(rules.size(), 3);
}

BOOST_AUTO_TEST_CASE(test_config_ptree_bundles)
{
  boost::property_tree::ptree ohrule;
  ohrule.add("UID", "OHRule");
  ohrule.add("IncludeFilter", ".*");
  ohrule.add("ExcludeFilter", "");
  ohrule.add("Name", "");
  ohrule.add("Parameters.OHPublishingParameters.UID", "OHParams");
  ohrule.add("Parameters.OHPublishingParameters.PublishInterval", 10);
  ohrule.add("Parameters.OHPublishingParameters.NumberOfSlots", 10);
  ohrule.add("Parameters.OHPublishingParameters.OHServer", "DF");
  ohrule.add("Parameters.OHPublishingParameters.ROOTProvider", "provider");

  boost::property_tree::ptree innerbundle;
  innerbundle.add_child("Rules.ConfigurationRule", ohrule);

  boost::property_tree::ptree isrule;
  isrule.add("UID", "ISRule");
  isrule.add("IncludeFilter", ".*");
  isrule.add("ExcludeFilter", ".*");
  isrule.add("Name", "Myname");
  isrule.add("Parameters.ISPublishingParameters.UID", "ISParams");
  isrule.add("Parameters.ISPublishingParameters.PublishInterval", 2);
  isrule.add("Parameters.ISPublishingParameters.NumberOfSlots", 1);
  isrule.add("Parameters.ISPublishingParameters.ISServer", "DF");
  
  boost::property_tree::ptree rulebundle;
  rulebundle.add_child("Rules.ConfigurationRule", isrule);
  rulebundle.add_child("LinkedBundles.ConfigurationRuleBundle", innerbundle);
 
  monsvc::PublishingController msvc(get_partition(), "fake");
  msvc.add_configuration_rules(rulebundle);
  
  std::vector<std::string> rules = msvc.get_rules();
  BOOST_REQUIRE_EQUAL(rules.size(), 2);
}

BOOST_AUTO_TEST_CASE(test_config_ptree_error_handling)
{
  //Wrong parameter
  boost::property_tree::ptree ohrule;
  ohrule.add("UID", "OHRule");
  ohrule.add("IncludeFilter", ".*");
  ohrule.add("ExcludeFilter", "");
  ohrule.add("Name", "");

  //Missing field
  boost::property_tree::ptree isrule;
  isrule.add("UID", "ISRule");
  isrule.add("ExcludeFilter", "");
  isrule.add("Name", "");
  isrule.add("Parameters.ISPublishingParameters.UID", "ISParams");
  isrule.add("Parameters.ISPublishingParameters.PublishInterval", 2);
  isrule.add("Parameters.ISPublishingParameters.NumberOfSlots", 1);
  isrule.add("Parameters.ISPublishingParameters.ISServer", "DF");

  //Wrong data
  boost::property_tree::ptree isrule2;
  isrule2.add("UID", "ISRule");
  isrule2.add("IncludeFilter", ".*");
  isrule2.add("ExcludeFilter", "");
  isrule2.add("Name", "");
  isrule2.add("Parameters.ISPublishingParameters.UID", "ISParams");
  isrule2.add("Parameters.ISPublishingParameters.PublishInterval", "Hello");
  isrule2.add("Parameters.ISPublishingParameters.NumberOfSlots", 1);
  isrule2.add("Parameters.ISPublishingParameters.ISServer", "DF");

  boost::property_tree::ptree rulebundle;
  rulebundle.add_child("Rules.ConfigurationRule", ohrule);
  rulebundle.add_child("Rules.ConfigurationRule", isrule);
  rulebundle.add_child("Rules.ConfigurationRule", isrule2);

  monsvc::PublishingController msvc(get_partition(), "fake");
  msvc.add_configuration_rules(rulebundle);

  std::vector<std::string> rules = msvc.get_rules();
  BOOST_REQUIRE_EQUAL(rules.size(), 0);
}


BOOST_AUTO_TEST_SUITE_END()

/* Local Variables: */
/* c-basic-offset:4 */
/* End: */
