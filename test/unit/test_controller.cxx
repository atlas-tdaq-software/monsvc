#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE monsvc PublishingController Tests
#define BOOST_TEST_MAIN

#include <map>
#include <mutex>
#include <string>
#include <boost/test/unit_test.hpp>

#include "ipc/partition.h"
#include "monsvc/ptr.h"
#include "monsvc/MonitoringService.h"
#include "monsvc/PublishingController.h"
#include "monsvc/PublisherBase.h"
#include "Registry.h"

#include "TH1.h"

using namespace monsvc;

namespace {

/**
 * Mock publisher that does not publish but simply counts objects
 * that were sent for publishing.
 */
class MockCountingPublisher : public PublisherBase {
public:

    MockCountingPublisher(const std::string& name_) : PublisherBase(), name(name_) {}

    bool knows(TObject*) override { return true; }
    bool knows(TH1*) override { return true; }
    bool knows(TGraph*) override { return true; }
    bool knows(TGraph2D*) override { return true; }
    bool knows(ISInfo*) override { return true; }
    void publish(const std::string& name, const Tag&, const Annotations&, TObject*) override {
        count("TObject", name);
    }
    void publish(const std::string& name, const Tag&, const Annotations&, TH1*) override {
        count("TH1", name);
    }
    void publish(const std::string& name, const Tag&, const Annotations&, TGraph*) override {
        count("TGraph", name);
    }
    void publish(const std::string& name, const Tag&, const Annotations&, TGraph2D*) override {
        count("TGraph2D", name);
    }
    void publish(const std::string& name, const Tag&, const Annotations&, ISInfo*) override {
        count("ISInfo", name);
    }

    void count(const std::string& type, const std::string& name) {
        std::lock_guard<std::mutex> lock(mutex);
        counters[type + "::" + this->name + "/" + name] ++;
    }

    const std::string name;
    std::mutex mutex;
    static std::map<std::string, unsigned> counters;
};

std::map<std::string, unsigned> MockCountingPublisher::counters;


class MockConfigurationRule : public ConfigurationRule {
public:
    MockConfigurationRule(const std::string& name, const NameFilter& filter,
                          unsigned interval, unsigned nslots,
                          const std::string& server=std::string())
        : ConfigurationRule(name, filter, interval, nslots, server)
    {}

    std::shared_ptr<PublisherBase>
    create_publisher(const IPCPartition& partition, const std::string& app_name) const override {
        return std::make_shared<MockCountingPublisher>(get_name());
    }
    const std::string params_str() const override { return "mock"; }
};


} // namespace

// macros are better because they can print actual line number
#define CHECK_COUNTER_SIZE(SIZE) BOOST_TEST(MockCountingPublisher::counters.size() == SIZE)
#define CHECK_COUNTER(name, value) BOOST_TEST(MockCountingPublisher::counters[name] == value)

/**
 * Test suite for the PublishingController class.
 */

class PublishingControllerFixture {
public:
    ~PublishingControllerFixture() {
        detail::Registry::instance().clear();
        MockCountingPublisher::counters.clear();
    }

    MonitoringService& mon_svc = MonitoringService::instance();
};

BOOST_FIXTURE_TEST_SUITE(PublishingControllerTest, PublishingControllerFixture)

BOOST_AUTO_TEST_CASE(test_single_rule)
{
    // simple test, controller configured with one all-inclusive rule

    // We don't need actual partition, but have to pass something to constructor
    IPCPartition partition("partition");
    const std::string app_name = "app";

    PublishingController ctrl(partition, app_name);

    NameFilter f1;
    MockConfigurationRule rule1("one", f1, 1, 1);
    ctrl.add_configuration_rule(rule1);

    mon_svc.register_object("h1", static_cast<TH1 *>(0));
    mon_svc.register_object("h2", static_cast<TH1 *>(0));

    ctrl.publish_all();

    // should have published two objects
    CHECK_COUNTER_SIZE(2);
    CHECK_COUNTER("TH1::one/h1", 1);
    CHECK_COUNTER("TH1::one/h2", 1);
}

BOOST_AUTO_TEST_CASE(test_exclusive_rules)
{
    // controller configured with two rules that are exclusive

    // We don't need actual partition, but have to pass something to constructor
    IPCPartition partition("partition");
    const std::string app_name = "app";

    PublishingController ctrl(partition, app_name);

    NameFilter f1(".*", "h2");
    MockConfigurationRule rule1("one", f1, 1, 1);
    ctrl.add_configuration_rule(rule1);

    NameFilter f2("h2");
    MockConfigurationRule rule2("two", f2, 1, 1);
    ctrl.add_configuration_rule(rule2);

    mon_svc.register_object("h1", static_cast<TH1 *>(0));
    mon_svc.register_object("h2", static_cast<TH1 *>(0));

    ctrl.publish_all();

    // should have published two objects
    CHECK_COUNTER_SIZE(2);
    CHECK_COUNTER("TH1::one/h1", 1);
    CHECK_COUNTER("TH1::two/h2", 1);
}

BOOST_AUTO_TEST_CASE(test_non_exclusive_rules)
{
    // controller configured with two rules that are not exclusive,
    // the rule with higher update rate takes precedence

    // We don't need actual partition, but have to pass something to constructor
    IPCPartition partition("partition");
    const std::string app_name = "app";

    PublishingController ctrl(partition, app_name);

    NameFilter f1(".*");
    MockConfigurationRule rule1("one", f1, 10, 1, "is_server");
    ctrl.add_configuration_rule(rule1);

    NameFilter f2("h2");
    MockConfigurationRule rule2("two", f2, 1, 1, "is_server");
    ctrl.add_configuration_rule(rule2);

    mon_svc.register_object("h1", static_cast<TH1 *>(0));
    mon_svc.register_object("h2", static_cast<TH1 *>(0));

    ctrl.publish_all();

    // should have published two objects
    CHECK_COUNTER_SIZE(2);
    CHECK_COUNTER("TH1::one/h1", 1);
    CHECK_COUNTER("TH1::two/h2", 1);
    CHECK_COUNTER("TH1::one/h2", 0);
}

BOOST_AUTO_TEST_CASE(test_non_exclusive_rules_different_servers)
{
    // controller configured with two rules that are not exclusive,
    // going to different servers they don't interfere.

    // We don't need actual partition, but have to pass something to constructor
    IPCPartition partition("partition");
    const std::string app_name = "app";

    PublishingController ctrl(partition, app_name);

    NameFilter f1(".*");
    MockConfigurationRule rule1("one", f1, 10, 1, "is_server_1");
    ctrl.add_configuration_rule(rule1);

    NameFilter f2("h2");
    MockConfigurationRule rule2("two", f2, 1, 1, "is_server_2");
    ctrl.add_configuration_rule(rule2);

    mon_svc.register_object("h1", static_cast<TH1 *>(0));
    mon_svc.register_object("h2", static_cast<TH1 *>(0));

    ctrl.publish_all();

    // should have published three objects
    CHECK_COUNTER_SIZE(3);
    CHECK_COUNTER("TH1::one/h1", 1);
    CHECK_COUNTER("TH1::two/h2", 1);
    CHECK_COUNTER("TH1::one/h2", 1);
}

BOOST_AUTO_TEST_CASE(test_non_exclusive_rules_empty_servers)
{
    // controller configured with two rules that are not exclusive,
    // when server names are empty they stay non-exclusive.

    // We don't need actual partition, but have to pass something to constructor
    IPCPartition partition("partition");
    const std::string app_name = "app";

    PublishingController ctrl(partition, app_name);

    NameFilter f1(".*");
    MockConfigurationRule rule1("one", f1, 10, 1, "");
    ctrl.add_configuration_rule(rule1);

    NameFilter f2("h2");
    MockConfigurationRule rule2("two", f2, 1, 1, "");
    ctrl.add_configuration_rule(rule2);

    mon_svc.register_object("h1", static_cast<TH1 *>(0));
    mon_svc.register_object("h2", static_cast<TH1 *>(0));

    ctrl.publish_all();

    // should have published three objects
    CHECK_COUNTER_SIZE(3);
    CHECK_COUNTER("TH1::one/h1", 1);
    CHECK_COUNTER("TH1::two/h2", 1);
    CHECK_COUNTER("TH1::one/h2", 1);
}

BOOST_AUTO_TEST_SUITE_END()
