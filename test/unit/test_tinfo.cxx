#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE monsvc TInfo Tests
#define BOOST_TEST_MAIN

#include <boost/test/unit_test.hpp>

#include "test/unit/test_online_fixture.h"
#include "test/common/test_monsvc.h"

#include "TInfo.h"

#include <is/info.h>
#include <is/infoany.h>
#include <is/infodictionary.h>
#include <is/infoiterator.h>
#include <is/criteria.h>

/**
 * Test suite for the TObject to ISInfo conversion using a generated dictionary.
 * XXX: need not use the IS server. Need just to check that the resulting object is OK.
 */

ClassImp(MyObject)


void MyObject::Print(const Option_t *) const
{
    std::cout << "MyObject: x = " << x << " y = " << y << std::endl;
}

BOOST_FIXTURE_TEST_SUITE(TInfoTest, OnlineTestFixture)

// XXX: (static) checks for all datatype sizes.
BOOST_AUTO_TEST_CASE(check_tinfo)
{
    const std::string obj_name = get_server_name() + ".MyTest";

    MyObject obj(1, 2.0, 3.0, 4, 5.0, std::vector<double>(1, 6), "7.0");
    monsvc::detail::TInfo info(&obj);

    // Write the object in the IS server.
    get_is_dictionary().checkin(obj_name, info);

    ISInfoAny isa;
    get_is_dictionary().findValue(obj_name,isa);
    
    // Check that we retrieve the same object.
    BOOST_REQUIRE_EQUAL(isa.type().name(), "MyObject");
    BOOST_REQUIRE_EQUAL(isa.countAttributes(), 5);

    // First attribute is an integer;
    BOOST_REQUIRE_EQUAL(isa.getAttributeType(), ISType::S32);
    int x;
    isa >> x;
    BOOST_CHECK_EQUAL(x, 1);

    BOOST_REQUIRE_EQUAL(isa.getAttributeType(), ISType::Float);
    float y;
    isa >> y;
    BOOST_CHECK_EQUAL(y, 2.0);
    
    BOOST_REQUIRE_EQUAL(isa.getAttributeType(), ISType::Double);
    double d;
    isa >> d;
    BOOST_CHECK_EQUAL(d, 3.0);

    /*
    bool found = false;
    ISInfoIterator ii(get_partition(), get_server_name(), ISCriteria(".*"));
    while( ii() ) {
        if (ii.name() == obj_name) {
            found = true;
            ISInfoAny isa;
            ii.value( isa );

            // Check that we retrieve the same object.
            BOOST_REQUIRE_EQUAL(isa.type().name(), "MyObject");
            BOOST_REQUIRE_EQUAL(isa.countAttributes(), 5);

            // First attribute is an integer;
            BOOST_REQUIRE_EQUAL(isa.getAttributeType(), ISType::S32);
            int x;
            isa >> x;
            BOOST_CHECK_EQUAL(x, 1);

            BOOST_REQUIRE_EQUAL(isa.getAttributeType(), ISType::Float);
            float y;
            isa >> y;
            BOOST_CHECK_EQUAL(y, 2.0);

            BOOST_REQUIRE_EQUAL(isa.getAttributeType(), ISType::Double);
            double d;
            isa >> d;
            BOOST_CHECK_EQUAL(d, 3.0);

            // FIXME : array members are not published*/
/*
            BOOST_CHECK_EQUAL(isa.isAttributeArray(), true);
            BOOST_CHECK_EQUAL(isa.getAttributeType(), ISType::S32);
            int a[10];
            size_t size = 10;
            isa.get(&a[0], size);
            BOOST_CHECK_EQUAL(size, 10);
            for (int i = 0; i < 10; i++) {
                BOOST_CHECK_EQUAL(a[i], 4 * i);
            }

            BOOST_CHECK_EQUAL(isa.isAttributeArray(), true);
            BOOST_CHECK_EQUAL(isa.getAttributeType(), ISType::Float);
            float b[10];
            size = 20;
            isa.get(&b[0], size);
            BOOST_CHECK_EQUAL(size, 20);
            for (int i = 0; i < 20; i++) {
                BOOST_CHECK_EQUAL(b[i], 5.0 * i);
            }


            BOOST_CHECK_EQUAL(isa.isAttributeArray(), true);
            BOOST_CHECK_EQUAL(isa.getAttributeType(), ISType::Double);
            double c[20];
            size = 20;
            isa.get(&c[0], size);
            BOOST_CHECK_EQUAL(size, 20);
            for (int i = 0; i < 20; i++) {
                BOOST_CHECK_EQUAL(c[i], 6.0);
            }

            BOOST_CHECK_EQUAL(isa.getAttributeType(), ISType::String);
            std::string s;
            isa >> s;
            BOOST_CHECK_EQUAL(s, "7.0");
  */
    /*     }
    }
    BOOST_CHECK_EQUAL(found, true);*/
}

BOOST_AUTO_TEST_SUITE_END()
