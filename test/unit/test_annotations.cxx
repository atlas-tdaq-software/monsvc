#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE monsvc Annotations Tests
#define BOOST_TEST_MAIN

#include <boost/test/unit_test.hpp>

#include <iostream>

#include "is/info.h"
#include "oh/OHRootReceiver.h"

#include "monsvc/MonitoringService.h"
#include "monsvc/ConfigurationRules.h"
#include "monsvc/PublishingController.h"

#include "OHPublisher.h"
#include "Registry.h"
#include "PeriodicScheduler.h"

#include "test/unit/test_online_fixture.h"

class AnnotationsTestFixture : public OnlineTestFixture {
public:
  ~AnnotationsTestFixture() {
    monsvc::detail::Registry::instance().clear();
  }
};

BOOST_FIXTURE_TEST_SUITE(AnnotationsTest, AnnotationsTestFixture)

BOOST_AUTO_TEST_CASE(check_create_add_remove_annotations)
{
  monsvc::Annotations annotations = {{"key","value"},};
  std::string name("info1");

  monsvc::MonitoringService::instance().register_object(name, 
                                                        annotations,
                                                        static_cast<ISInfo *>(0));
  
  auto obj = 
    monsvc::detail::Registry::instance().find_object(name, monsvc::Tag());

  // Check the static annotation exist
  BOOST_CHECK_EQUAL(obj->annotations().size(), 1); 

  // Add one annotation
  obj->add_annotation(monsvc::Annotation("test","second"));
  BOOST_CHECK_EQUAL(obj->annotations().size(), 2); 

  // Remove one annotation
  obj->remove_annotation(monsvc::Annotation("key","value"));
  BOOST_CHECK_EQUAL(obj->annotations().size(), 1); 
  BOOST_CHECK_EQUAL(obj->annotations()[0].first, "test"); 

  // Add a list of annotations
  monsvc::Annotations news = {{"1","2"},{"3","4"}};
  obj->add_annotations(news);
  BOOST_CHECK_EQUAL(obj->annotations().size(), 3); 

  // And then remove them
  obj->remove_annotations(news);
  BOOST_CHECK_EQUAL(obj->annotations().size(), 1); 
}

BOOST_AUTO_TEST_CASE(check_publish_annotations)
{

  const std::string provider_name = "provider_name";

  // Create the publisher by using a rule.
  std::shared_ptr<monsvc::ConfigurationRule> rule = 
    monsvc::ConfigurationRule::from(
                                    "rule:.*/=>oh:(2,1," + get_server_name() +
                                    "," + provider_name + ")");
  
  // Check that the publisher has the right type.
  std::shared_ptr<monsvc::PublisherBase> pub =
    rule->create_publisher(get_partition(), get_app_name());
  std::shared_ptr<monsvc::OHPublisher> ohpub =
    std::dynamic_pointer_cast<monsvc::OHPublisher>(pub);

  const std::string hname("histo");
  TH1F histo(hname.c_str(), hname.c_str(), 100, 0., 100.);

  monsvc::Tag nulltag;
  monsvc::Annotations ann = {{"key","value"},};
  ohpub->publish(hname, nulltag, ann, &histo);

  // And check that we can retrieve it from the OH server.
  OHRootHistogram recvd_histo = 
    OHRootReceiver::getRootHistogram(get_partition(), get_server_name(),
                                     provider_name, hname);
  BOOST_CHECK_EQUAL(recvd_histo.annotations.size(), 1);

}

BOOST_AUTO_TEST_CASE(check_register_publish_annotations)
{
  monsvc::Annotations annotations = {{"key","value"},};
  
  // Publish a histogram.
  const std::string hname("histo");
  TH1F histo(hname.c_str(), hname.c_str(), 100, 0., 100.);

  monsvc::MonitoringService::instance().register_object(hname, 
                                                        annotations,
                                                        &histo);
  
  // Create publisher
  const std::string provider_name = "provider_name";
  auto ohpub = std::make_shared<monsvc::OHPublisher>(get_partition(), 
                                                   get_server_name(),
                                                   provider_name);
  
  // Publish everything
  monsvc::NameFilter filter(".*");
  monsvc::detail::PeriodicScheduler::PeriodicTask task(ohpub, filter);
  task.publish(0, 1);
  
  // And check that we can retrieve it from the OH server.
  OHRootHistogram recvd_histo = 
    OHRootReceiver::getRootHistogram(get_partition(), get_server_name(),
                                     provider_name, hname);
  BOOST_CHECK_EQUAL(recvd_histo.annotations.size(), 1);

}

BOOST_AUTO_TEST_CASE(check_final_annotations)
{
  const std::string provider_name = "provider_name";
  
  // Create the publisher by using a rule.
  std::shared_ptr<monsvc::ConfigurationRule> rule = 
    monsvc::ConfigurationRule::from(
                                    "rule:.*/=>oh:(1,1," + get_server_name() +
                                    "," + provider_name + ")");

  monsvc::PublishingController pub(get_partition(), get_app_name());
  pub.add_configuration_rule(*rule);

  // Publish a histogram.
  const std::string hname("histo");
  TH1F histo(hname.c_str(), hname.c_str(), 100, 0., 100.);
  monsvc::MonitoringService::instance().register_object(hname, &histo);

  auto obj = 
    monsvc::detail::Registry::instance().find_object(hname, monsvc::Tag());

  // The should be no annotations
  auto ann = obj->annotations();
  BOOST_CHECK_EQUAL(ann.size(), 0);

  // Check final publication
  pub.publish_all();
  OHRootHistogram recvd_histo = 
    OHRootReceiver::getRootHistogram(
                                     get_partition(), get_server_name(),
                                     provider_name, hname);
  
  BOOST_CHECK_EQUAL(recvd_histo.annotations.size(), 1);
  BOOST_CHECK_EQUAL(recvd_histo.annotations[0].first, "FINAL");
  BOOST_CHECK_EQUAL(recvd_histo.annotations[0].second, "FINAL");

  // Check the temporary annotations was removed
  ann = obj->annotations();
  BOOST_CHECK_EQUAL(ann.size(), 0);
}

BOOST_AUTO_TEST_SUITE_END()
