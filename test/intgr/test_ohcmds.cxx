#include "monsvc/PublishingController.h"
#include "monsvc/MonitoringService.h"
#include <ipc/partition.h>
#include <ipc/core.h>
#include <oh/OHRootMutex.h>

#include <TH1F.h>
#include <TH2F.h>
#include <THashList.h>
#include <TAxis.h>

#include <string>
#include <cstdlib>
#include <csignal>
#include <unistd.h>
#include <mutex>

static bool running = true;


void deflate_labels(const std::string&, TH1* h){
  std::lock_guard<std::mutex> l(OHRootMutex::getMutex());
  h->LabelsDeflate();
}

void signal_handler(int)
{
  running = false;
}

int main(int argc, char *argv[])
{
    using namespace monsvc;
    using namespace boost;
    
    std::vector<TH1F *> histos;


    // get the registry
    MonitoringService& ms = MonitoringService::instance();

    //Create histos
    for(unsigned int i=0;i<1000;i++){
      TH1F* h = new TH1F(std::to_string(i).c_str(),std::to_string(i).c_str(),
                         100,-0.5,99.5);
      h->SetCanExtend(TH1::kAllAxes);
      histos.push_back(h);
      ms.register_object(h->GetName(), h, true, deflate_labels);
    }

    // if we are in a partition, run the
    if(::getenv("TDAQ_PARTITION") != 0) {

        // initialize IPC
        IPCCore::init(argc, argv);

        IPCPartition partition(::getenv("TDAQ_PARTITION"));
        std::string app_name(::getenv("TDAQ_APPLICATION_NAME"));

        PublishingController pc(partition, app_name);

        pc.add_configuration_rule(
                *ConfigurationRule::from("Histogramming:.*/=>oh:(5,1,DF,myprovider)"));
        // start the publishing
        std::cout << "Start publishing" << std::endl;
        pc.start_publishing();
        
        // Install a signal handler
        running = true;
        std::signal(SIGINT, signal_handler);
        
        std::mutex& ohmutex = OHRootMutex::getMutex();

        while(running){
          for(auto &h: histos){
            std::lock_guard<std::mutex> l(ohmutex);
            h->Fill(std::to_string(std::rand()%1000000).c_str(),1);
          }
          
          if(std::rand()%100 == 0){
            for(auto &h: histos){
              std::lock_guard<std::mutex> l(ohmutex);
              h->Reset();
              h->GetXaxis()->GetLabels()->Clear();
              h->LabelsDeflate("X");          
            }
          }
        }
        std::cout << "Stop publishing" << std::endl;
        pc.stop_publishing();

    }
}
