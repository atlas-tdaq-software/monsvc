#ifndef HLTSV_H_
#define HLTSV_H_

#include <tbb/atomic.h>
#include <is/info.h>

namespace daq {

  class HLTSV : public ISInfo {
  public:
    HLTSV ()
     : ISInfo("HLTSV"), LVL1Events(), AssignedEvents(), ProcessedEvents(), Timeouts(), ProcessingNodesInitial(), ProcessingNodesDisabled(), ProcessingNodesEnabled(), ProcessingNodesAdded(), Rate()
    {}

    tbb::atomic<uint64_t>  LVL1Events;
    tbb::atomic<uint64_t>  AssignedEvents;
    tbb::atomic<uint64_t>  ProcessedEvents;
    tbb::atomic<uint64_t>  Timeouts;
    tbb::atomic<uint32_t>  ProcessingNodesInitial;
    tbb::atomic<uint32_t>  ProcessingNodesDisabled;
    tbb::atomic<uint32_t>  ProcessingNodesEnabled;
    tbb::atomic<uint32_t>  ProcessingNodesAdded;
    float  Rate;
    void set_Rate(float arg) { Rate = arg; }
    float get_Rate() const { return Rate; }
    void add_Rate(float arg) { Rate += arg; }


    void publishGuts(ISostream& s) {
       s <<  LVL1Events
         << AssignedEvents
         << ProcessedEvents
         << Timeouts
         << ProcessingNodesInitial
         << ProcessingNodesDisabled
         << ProcessingNodesEnabled
         << ProcessingNodesAdded
         << Rate ;
    }

    void refreshGuts(ISistream& s) {
       s >>  (uint64_t& )LVL1Events
         >> (uint64_t& )AssignedEvents
         >> (uint64_t& )ProcessedEvents
         >> (uint64_t& )Timeouts
         >> (uint32_t& )ProcessingNodesInitial
         >> (uint32_t& )ProcessingNodesDisabled
         >> (uint32_t& )ProcessingNodesEnabled
         >> (uint32_t& )ProcessingNodesAdded
         >> Rate ;
    }
    std::ostream& print(std::ostream& os) const {
        os <<  "  LVL1Events:\t" << LVL1Events << '\n'
           << "  AssignedEvents:\t" << AssignedEvents << '\n'
           << "  ProcessedEvents:\t" << ProcessedEvents << '\n'
           << "  Timeouts:\t" << Timeouts << '\n'
           << "  ProcessingNodesInitial:\t" << ProcessingNodesInitial << '\n'
           << "  ProcessingNodesDisabled:\t" << ProcessingNodesDisabled << '\n'
           << "  ProcessingNodesEnabled:\t" << ProcessingNodesEnabled << '\n'
           << "  ProcessingNodesAdded:\t" << ProcessingNodesAdded << '\n'
           << "  Rate:\t" << Rate << '\n' ;
        os << std::endl;
        return os;
    }
  };
}

#endif
