#include "monsvc/ptr.h"
#include "monsvc/PublishingController.h"
#include "PeriodicScheduler.h"
#include "monsvc/MonitoringService.h"
#include "monsvc/NameFilter.h"
#include "FilePublisher.h"
#include "OstreamPublisher.h"

#include "Registry.h"

#include <TObject.h>
#include <TH1.h>
#include <TGraph.h>
#include <TFile.h>

#include <is/info.h>

#include <memory>
#include <mutex>

#include <iostream>

/**
 * Usage example of the library.
 */

//
// HLTSV.h defines the HLTSV class, automatically
// generated from a schema and all discrete types
// defined as atomic<>.
//
#include "HLTSV.h"

//
// This defines a TObject derived class MyObject.
// We put it in the header file for rootcint.
//
#include "test/common/test_monsvc.h"

// A hand-written class, no schema will be available
// at run-time. Not recommended but shows how to
// add methods like g() that can be executed under a lock.
//
class MyInfo : public ISInfo {
public:
    MyInfo()
        : ISInfo("MyInfo"), x(), y()
    {}

    ~MyInfo()
    {}

    // data
    int x;
    float y;

    void g() {
        std::cout << "calling g()" << std::endl;
        x = 10;
        y = 10 * 3.141;
    }

    // manual implementation of required methods
    void publishGuts(ISostream &s) {
        s << x << y;
    }
    void refreshGuts(ISistream &s) {
        s >> x >> y;
    }

    std::ostream& print(std::ostream& os) const
    {
        os << "x = " << x << " y = " << y << std::endl;
        return os;
    }
};

void MyObject::Print(const Option_t *) const
{
    std::cout << "MyObject: x = " << x << " y = " << y << std::endl;
}


// ROOT magic for MyObject
ClassImp(MyObject)

//
// A simple free callback function.
// Fills the 'obj' with some values. When it is called
// 'obj' is locked.
//
void fill_it(const std::string& name, MyInfo *obj)
{
    std::cout << "fill_it called for " << name << std::endl;
    obj->x = 10;
    obj->y = 3.141;
}

// simple helper to demonstract Registry::for_each() function
void show_name(monsvc::detail::Registry::Entry obj)
{
    std::cout << obj->name() << std::endl;
}

int main(int argc, char *argv[])
{
    using namespace monsvc;
    using namespace boost;

    // declare the various objects
    MyInfo info;
    MyInfo info2;
    MyObject obj;

    daq::HLTSV hltinfo;

    // get the registry
    MonitoringService& ms = MonitoringService::instance();

    // Note that we don't have a raw pointer to the TH1F, the Registry will delete it when done
    ptr<TH1F> ph(ms.register_object("/EXPERT/histo", new TH1F("tst","tst", 100, 0, 100), true));

    // registering stack based objects
    ptr<MyInfo> pi(ms.register_object("info", &info));
    ptr<MyObject> po(ms.register_object("object", &obj));

    ptr<daq::HLTSV> hlt(ms.register_object("HLTSV", &hltinfo));

    // an object with a callback to fill it
    ptr<MyInfo> cb(ms.register_object("cb", &info2, false,
            static_cast<std::function<void(const std::string&, MyInfo *)> >(fill_it)));

    // this call is locked and protected
    ph->Fill(10);

    // these are as well
    pi->g();
    po->f();

    {
        unique_lock<ptr<MyInfo> > lock(pi);
        hlt->Rate += 3.4;
    }

    {
        // internally defined scoped_lock
        ptr<MyInfo>::scoped_lock lock(pi);
        hlt->Rate += 3.4;

        ptr<TH1F>::scoped_lock lock2(ph);
        // get raw object to avoid dead lock and for efficiency
        TH1F *h = ph.get();
        h->Fill(20);
    }

    {
        // multiple object locks...
        std::lock(pi, ph);

        ptr<MyInfo>::scoped_lock lock1(pi, std::adopt_lock);
        ptr<TH1F>::scoped_lock lock2(ph, std::adopt_lock);

        // update both pi and ph together.

    }

    // atomic...
    hltinfo.ProcessedEvents++;

    // atomic
    hltinfo.ProcessedEvents += 10;

    // not protected
    hlt->Rate += 3.4;

    ptr<MyInfo> pother(pi);

    ptr<MyInfo> empty;

    // ptr<> of the same type can be assigned
    empty = pother;

    // Does not work:
    // cannot convert ptr<T> to ptr<BASE_T>
    //
    // ptr<ISInfo> pbase(pi);
    //

    // The only way to access the Registry
    {
        monsvc::detail::Registry& r = monsvc::detail::Registry::instance();
        std::cout << "Registered objects:" << std::endl << "-----------------" << std::endl;
        r.for_each(static_cast<std::function<void(monsvc::detail::Registry::Entry)> >(show_name));
        std::cout << "-----------------" << std::endl;
    }

    // Publisher examples
    NameFilter filter;

    // ostream publisher
    {
        std::shared_ptr<OstreamPublisher> pub(
                std::make_shared<OstreamPublisher>(std::ref(std::cout)));
        monsvc::detail::PeriodicScheduler::PeriodicTask task(pub, filter);
        task.publish(0, 1);
    }

    // TFile publisher
    {
        TFile f("out.root", "RECREATE");
        std::shared_ptr<FilePublisher> pub(std::make_shared<FilePublisher>(&f));
        monsvc::detail::PeriodicScheduler::PeriodicTask task(pub, filter);
        task.publish(0, 1);
        f.Write();
        f.Close();
    }


    // if we are in a partition, run the
    // ISPublisher
    if(getenv("TDAQ_PARTITION") != 0) {

        // initialize IPC
        IPCCore::init(argc, argv);

        IPCPartition partition(getenv("TDAQ_PARTITION"));
        std::string app_name(getenv("TDAQ_APPLICATION_NAME"));

        PublishingController pc(partition, app_name);


        //
        // do the configuration by hand, most specific
        // regular expression comes last, least specific first
        //
        pc.add_configuration_rule(*ConfigurationRule::from("DFObjects:.*/=>is:(2,DF)"));
        pc.add_configuration_rule(
                *ConfigurationRule::from("Histogramming:.*/=>oh:(5,DF,provider_name)"));

        // start the publishing
        std::cout << "Start publishing" << std::endl;
        pc.start_publishing();
        for(int i = 0; i < 10; i++) {
            sleep(2);
            ph->Fill(i);
            hlt->Rate += 1.0;
            po->x *= 2;
        }
        // stop it
        std::cout << "Stop publishing" << std::endl;
        pc.stop_publishing();

        std::cout << "Restarting" << std::endl;
        pc.start_publishing();
        for(int i = 0; i < 1000; i++) {
            std::ostringstream s;
            s << "/DEBUG/SomeThing/Histo" << i;
            ptr<TH1F> h(ms.register_object(
                    s.str(), new TH1F(s.str().c_str(), s.str().c_str(), 100, 0., 1000.), true));
            h->Fill(i);
        }
        sleep(20);
        std::cout << "Stop again" << std::endl;
        pc.stop_publishing();
    }

    monsvc::detail::Registry::instance().clear();

}
