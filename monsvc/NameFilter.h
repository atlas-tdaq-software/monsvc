/*
 * PublishingFilter.h
 *
 *  Created on: Oct 22, 2012
 *      Author: ctalau
 */

#ifndef MONSVC_NAME_FILTER_H_
#define MONSVC_NAME_FILTER_H_

#include <boost/regex.hpp>
#include <string>

namespace monsvc {

    /**
     * @brief A filter on the object names.
     *
     * It specifies include and exclude filters as regular expressions.
     */
    class NameFilter {
    public:
        /**
         * @brief Constructs a filter for object names.
         *
         * By default, the constructed filter matches everything. If only the @a include_filter is
         * given, it matches the names matched by that regular expression. The parameters are
         * regular expressions with BOOST syntax.
         */
        NameFilter(const std::string& include_filter=".*", const std::string& exclude_filter="");

        /**
         * @brief Check whether the \a name is matched by the filter.
         *
         * Matches the name if and only if the first regular expression matches it and the second
         * one does not.
         */
        bool matches(const std::string& name) const;

        /**
         * @brief Returns a string representation of the filter.
         */
        const std::string str() const;

    private:
        boost::regex m_include_filter;
        boost::regex m_exclude_filter;
    };

} /* namespace monsvc */
#endif /* MONSVC_NAME_FILTER_H_ */
