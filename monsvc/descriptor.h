// -*- c++ -*-
#ifndef MONSVC_DESCRIPTOR_H_
#define MONSVC_DESCRIPTOR_H_

#include "monsvc/PublisherBase.h"
#include "monsvc/CommanderBase.h"
#include "monsvc/Tag.h"

#include <memory>
#include <mutex>
#include <vector>
#include <algorithm>
#include <functional>

namespace monsvc {

  /*
   * @brief Base class for descriptor, hiding type information.
   *
   * The Registry object stores pointers to
   * this base class only.
   *
   * The underlying type can be recovered by passing a
   * visitor object of a derived class of PublisherBase
   * and implementing the visitor methods there.
   *
   * This is an implementation detail that should only concern
   * the author of a PublisherBase subclass.
   */
  class descriptor_base : public std::mutex {
  public:
    /** @brief Constructs an object with the given @c name. */
    explicit descriptor_base(const std::string& name, const Tag& tag,
                             const Annotations& ann) 
      : m_name(name), m_tag(tag), m_annotations(ann) {}
    virtual ~descriptor_base() {}

    /** @brief Getter for the name of the object. */
    const std::string& name() const { return m_name; }
    
    /** @brief Getter for the tag of the object. */
    const Tag& tag_info() const { return m_tag; }
    
    /** @brief Getter for the tag status of the object. */
    bool has_tag() const { return m_tag.set; }

    /** @brief Getter for the tag value of the object. */
    int tag() const { return m_tag.value; }

    /** @brief Returns the annotations of the object. */
    const Annotations& annotations() const;

    /** @brief Adds one annotation to the object. */
    void add_annotation(const Annotation&);

    /** @brief Adds annotations to the object. */
    void add_annotations(const Annotations&);

    /** @brief Removes a specific annotation from the object. */
    void remove_annotation(const Annotation&);

    /** @brief Removes annotations from the object. */
    void remove_annotations(const Annotations&);
       
    /** @brief Removes all annotations from the object. */
    void reset_annotations();

    /**
     * @brief Method to accept the @c visitor.
     *
     * @param visitor the visitor used to publish the object represented by this descriptor.
     */
    virtual void publish(PublisherBase& visitor) = 0;

    /**
     * @brief Method to accept the @c visitor.
     *
     * @param visitor the visitor used to execute commands on the object represented by this descriptor.
     */
    virtual void command(CommanderBase& visitor, const Command& cmd) = 0;
  protected:
    //friend class Registry;
    std::string  m_name;  ///< The name of the object.
    Tag m_tag; ///< The tag of the object
    Annotations m_annotations; ///< The annotations
  };

  /*
   * @brief The fully typed descriptor.
   *
   * This contains a pointer to the real object, a flag if the
   * descriptor owns this object, and a possible callback function
   * to fill the object.
   */
  template<class T>
  class descriptor :  public descriptor_base
  {
  public:
    /** @brief Creates a descriptor for object @c obj. */
    descriptor(const std::string& name, const Tag& tag,
               const Annotations& ann, T *obj, bool own,
               std::function<void (const std::string&, T*)> precallback,
               std::function<void (const std::string&, T*)> postcallback) :
      descriptor_base(name, tag, ann), m_obj(obj), 
      m_own(own), m_precallback(precallback),
      m_postcallback(postcallback) {}

    ~descriptor() { if(m_own) delete m_obj; }

    /** @brief Getter for the underlying object. */
    T *object() const { return m_obj; }

    virtual void publish(PublisherBase& visitor) override
    {
      if (m_precallback && visitor.knows(m_obj)) {
        std::lock_guard<std::mutex> lock(*this);
        m_precallback(m_name, m_obj);
      }
      
      // We cannot lock the object during publication
      // In fact the OH mutex will be locked (for histograms).
      // On the other hand, the user code will possibly
      // lock mutexes in the opposite order (OH first, object after)
      // Hence, if we lock the object we would potentially cause
      // a deadlock
      
      Annotations a;
      {
        // We cannot afford to have the annotation vector changed during
        // publication. On the other hand we cannot lock the object
        // (see above). Therefore we need to make a copy.
        std::lock_guard<std::mutex> lock(*this);
        if (!m_annotations.empty()) {
          a = m_annotations;
        }
      }
      
      visitor.publish(m_name, m_tag, a, m_obj);

      if (m_postcallback && visitor.knows(m_obj)) {
        std::lock_guard<std::mutex> lock(*this);
        m_postcallback(m_name, m_obj);
      }
    }

    virtual void command(CommanderBase& visitor, const Command& cmd) override
    {
      std::lock_guard<std::mutex> lock(*this);
      visitor.execute(cmd, m_obj);
    }
    
  private:
    T            *m_obj;
    bool          m_own;
    std::function<void (const std::string&, T*)>       m_precallback;
    std::function<void (const std::string&, T*)>       m_postcallback;
  };

}

#endif  // MONSVC_DESCRIPTOR_H_
