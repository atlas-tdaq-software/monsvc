// this is -*- c++ -*-
#ifndef MONSVC_TISBUFFER_H_
#define MONSVC_TISBUFFER_H_

#include <is/info.h>

#include <TBuffer.h>
#include <TString.h>

#include <boost/static_assert.hpp>

namespace monsvc {

    /*
     * NOTE: We are providing a bridge between two frameworks (IS and ROOT),
     * each with its own portable fixed size integer types. So, we depend
     * on them being compatible.
     */
    BOOST_STATIC_ASSERT(sizeof(Long64_t) == sizeof(is::id2type<ISType::S64>::type));
    BOOST_STATIC_ASSERT(sizeof(ULong64_t) == sizeof(is::id2type<ISType::U64>::type));

    /*
     * @brief Adapter between TBuffer and ISostream.
     *
     * This class is used to send TObject instances to an IS server.
     */
    class TISBuffer : public TBuffer {
    public:
        /** @brief Constructs a TISBuffer that forwards to @c os.*/
        TISBuffer(ISostream& os)
            : TBuffer(kWrite),
              m_os(os)
        {}

        ~TISBuffer() {}
        virtual Bool_t CheckObject(const TObject*) { return kFALSE; }
        virtual Bool_t CheckObject(const void*, const TClass*) { return kFALSE; }
        virtual Int_t ReadBuf(void*, Int_t) { return 0; }
        virtual void WriteBuf(const void*, Int_t) {}
        virtual char* ReadString(char*, Int_t) { return 0; }
        virtual void WriteString(const char* x) { std::string s(x); m_os << s; }
        virtual Int_t GetVersionOwner() const { return 0; }
        virtual Int_t GetMapCount() const { return 0; }
        virtual void GetMappedObject(UInt_t, void*&, TClass*&) const {}
        virtual void MapObject(const TObject*, UInt_t) {}
        virtual void MapObject(const void*, const TClass*, UInt_t) {}
        virtual void Reset() {}
        virtual void InitMap() {}
        virtual void ResetMap() {}
        virtual void SetReadParam(Int_t) {}
        virtual void SetWriteParam(Int_t) {}
        virtual Int_t CheckByteCount(UInt_t, UInt_t, const TClass*) { return 0; }
        virtual Int_t CheckByteCount(UInt_t, UInt_t, const char*) { return 0; }
        virtual void SetByteCount(UInt_t, Bool_t) {}
        virtual void SkipVersion(const TClass*) {}
        virtual Version_t ReadVersion(UInt_t*, UInt_t*, const TClass*) { return 0; }
        virtual Version_t ReadVersionForMemberWise(const TClass*) { return 0; }
        virtual Version_t ReadVersionNoCheckSum(UInt_t*, UInt_t*) { return 0; }
        virtual UInt_t WriteVersion(const TClass*, Bool_t) { return 0; }
        virtual UInt_t WriteVersionMemberWise(const TClass*, Bool_t) { return 0;}
        virtual void* ReadObjectAny(const TClass*) { return 0; }
        virtual void SkipObjectAny() {}
        virtual void TagStreamerInfo(TVirtualStreamerInfo*) {}
        virtual void IncrementLevel(TVirtualStreamerInfo*) {}
        // ROOT6
        virtual void SetStreamerElementNumber(TStreamerElement*,Int_t) {}
        // ROOT5
        virtual void SetStreamerElementNumber(Int_t) {}
        virtual void DecrementLevel(TVirtualStreamerInfo*) {}
        virtual void ClassBegin(const TClass*, Version_t) {}
        virtual void ClassEnd(const TClass*) {}
        virtual void ClassMember(const char*, const char*, Int_t, Int_t) {}
        virtual TVirtualStreamerInfo* GetInfo() { return 0; }
        virtual TClass* ReadClass(const TClass*, UInt_t*) {return 0; }
        virtual void WriteClass(const TClass*) {}
        virtual TObject* ReadObject(const TClass*) { return 0;}
        virtual void WriteObject(const TObject*) {}
        virtual Int_t WriteObjectAny(const void*, const TClass*) { return 0; }
        virtual UShort_t GetPidOffset() const { return 0; }
        virtual void SetPidOffset(UShort_t) {}
        virtual Int_t GetBufferDisplacement() const { return 0;}
        virtual void SetBufferDisplacement() {}
        virtual void SetBufferDisplacement(Int_t) {}
        virtual void ReadFloat16(Float_t*, TStreamerElement*) {}
        virtual void WriteFloat16(Float_t*, TStreamerElement*) {}
        virtual void ReadDouble32(Double_t*, TStreamerElement*) {}
        virtual void WriteDouble32(Double_t*, TStreamerElement*) {}
        virtual void ReadWithFactor(Float_t*, Double_t, Double_t) {}
        virtual void ReadWithNbits(Float_t*, Int_t) {}
        virtual void ReadWithFactor(Double_t*, Double_t, Double_t) {}
        virtual void ReadWithNbits(Double_t*, Int_t) {}
        virtual Int_t ReadArray(Bool_t*&) { return 0; }
        virtual Int_t ReadArray(Char_t*&) { return 0; }
        virtual Int_t ReadArray(UChar_t*&) { return 0; }
        virtual Int_t ReadArray(Short_t*&) { return 0; }
        virtual Int_t ReadArray(UShort_t*&) { return 0; }
        virtual Int_t ReadArray(Int_t*&) { return 0; }
        virtual Int_t ReadArray(UInt_t*&) { return 0; }
        virtual Int_t ReadArray(Long_t*&) { return 0; }
        virtual Int_t ReadArray(ULong_t*&) { return 0; }
        virtual Int_t ReadArray(Long64_t*&) { return 0; }
        virtual Int_t ReadArray(ULong64_t*&) { return 0; }
        virtual Int_t ReadArray(Float_t*&) { return 0; }
        virtual Int_t ReadArray(Double_t*&) { return 0; }
        virtual Int_t ReadArrayFloat16(Float_t*&, TStreamerElement*) { return 0; }
        virtual Int_t ReadArrayDouble32(Double_t*&, TStreamerElement*) { return 0; }
        virtual Int_t ReadStaticArray(Bool_t*) { return 0; }
        virtual Int_t ReadStaticArray(Char_t*) { return 0; }
        virtual Int_t ReadStaticArray(UChar_t*) { return 0; }
        virtual Int_t ReadStaticArray(Short_t*) { return 0; }
        virtual Int_t ReadStaticArray(UShort_t*) { return 0; }
        virtual Int_t ReadStaticArray(Int_t*) { return 0; }
        virtual Int_t ReadStaticArray(UInt_t*) { return 0; }
        virtual Int_t ReadStaticArray(Long_t*) { return 0; }
        virtual Int_t ReadStaticArray(ULong_t*) { return 0; }
        virtual Int_t ReadStaticArray(Long64_t*) { return 0; }
        virtual Int_t ReadStaticArray(ULong64_t*) { return 0; }
        virtual Int_t ReadStaticArray(Float_t*) { return 0; }
        virtual Int_t ReadStaticArray(Double_t*) { return 0; }
        virtual Int_t ReadStaticArrayFloat16(Float_t*, TStreamerElement*) { return 0; }
        virtual Int_t ReadStaticArrayDouble32(Double_t*, TStreamerElement*) { return 0; }
        virtual void ReadFastArray(Bool_t*, Int_t) {}
        virtual void ReadFastArray(Char_t*, Int_t) {}
        virtual void ReadFastArrayString(Char_t*, Int_t) {}
        virtual void ReadFastArray(UChar_t*, Int_t) {}
        virtual void ReadFastArray(Short_t*, Int_t) {}
        virtual void ReadFastArray(UShort_t*, Int_t) {}
        virtual void ReadFastArray(Int_t*, Int_t) {}
        virtual void ReadFastArray(UInt_t*, Int_t) {}
        virtual void ReadFastArray(Long_t*, Int_t) {}
        virtual void ReadFastArray(ULong_t*, Int_t) {}
        virtual void ReadFastArray(Long64_t*, Int_t) {}
        virtual void ReadFastArray(ULong64_t*, Int_t) {}
        virtual void ReadFastArray(Float_t*, Int_t) {}
        virtual void ReadFastArray(Double_t*, Int_t) {}
        virtual void ReadFastArrayFloat16(Float_t*, Int_t, TStreamerElement*) {}
        virtual void ReadFastArrayDouble32(Double_t*, Int_t, TStreamerElement*) {}
        virtual void ReadFastArray(void*, const TClass*, Int_t, TMemberStreamer*, const TClass*) {}
        virtual void ReadFastArray(void**, const TClass*, Int_t, Bool_t, TMemberStreamer*, const TClass*) {}
        virtual void ReadFastArrayWithFactor(Float_t*, Int_t, Double_t, Double_t) {}
        virtual void ReadFastArrayWithNbits(Float_t*, Int_t, Int_t) {}
        virtual void ReadFastArrayWithFactor(Double_t*, Int_t, Double_t, Double_t) {}
        virtual void ReadFastArrayWithNbits(Double_t*, Int_t, Int_t) {}
        virtual void WriteArray(const Bool_t* x, Int_t c) { m_os.put(x,c); }
        virtual void WriteArray(const Char_t* x, Int_t c) { m_os.put(x,c);}
        virtual void WriteArray(const UChar_t* x, Int_t c) { m_os.put(x,c);}
        virtual void WriteArray(const Short_t* x, Int_t c) { m_os.put(x,c);}
        virtual void WriteArray(const UShort_t* x, Int_t c) { m_os.put(x,c);}
        virtual void WriteArray(const Int_t* x, Int_t c) { m_os.put(x,c);}
        virtual void WriteArray(const UInt_t* x, Int_t c) { m_os.put(x,c);}
        virtual void WriteArray(const Long_t* x, Int_t c) { m_os.put(x,c);}
        virtual void WriteArray(const ULong_t* x, Int_t c) { m_os.put(x,c);}
        virtual void WriteArray(const Long64_t* x, Int_t c) {
            m_os.put(reinterpret_cast<const is::id2type<ISType::S64>::type *>(x),c);
        }
        virtual void WriteArray(const ULong64_t* x, Int_t c) {
            m_os.put(reinterpret_cast<const is::id2type<ISType::U64>::type *>(x),c);
        }
        virtual void WriteArray(const Float_t* x, Int_t c) { m_os.put(x,c);}
        virtual void WriteArray(const Double_t* x, Int_t c) { m_os.put(x,c);}
        virtual void WriteArrayFloat16(const Float_t*, Int_t, TStreamerElement*) {}
        virtual void WriteArrayDouble32(const Double_t*, Int_t, TStreamerElement*) {}
        virtual void WriteFastArray(const Bool_t* x, Int_t c) { m_os.put(x,c);}
        virtual void WriteFastArray(const Char_t* x, Int_t c) { m_os.put(x,c);}
        virtual void WriteFastArrayString(const Char_t* x, Int_t c) { m_os.put(x,c);}
        virtual void WriteFastArray(const UChar_t* x, Int_t c) { m_os.put(x,c);}
        virtual void WriteFastArray(const Short_t* x, Int_t c) { m_os.put(x,c);}
        virtual void WriteFastArray(const UShort_t* x, Int_t c) { m_os.put(x,c);}
        virtual void WriteFastArray(const Int_t* x, Int_t c) { m_os.put(x,c);}
        virtual void WriteFastArray(const UInt_t* x, Int_t c) { m_os.put(x,c);}
        virtual void WriteFastArray(const Long_t* x, Int_t c) { m_os.put(x,c);}
        virtual void WriteFastArray(const ULong_t* x, Int_t c) { m_os.put(x,c);}
        virtual void WriteFastArray(const Long64_t* x, Int_t c) {
            // XXX: Cast between pointers is undefined behavior.
            m_os.put(reinterpret_cast<const is::id2type<ISType::S64>::type *>(x),c);
        }
        virtual void WriteFastArray(const ULong64_t* x, Int_t c) {
            m_os.put(reinterpret_cast<const is::id2type<ISType::U64>::type *>(x),c);
        }
        virtual void WriteFastArray(const Float_t* x, Int_t c) { m_os.put(x,c);}
        virtual void WriteFastArray(const Double_t* x, Int_t c) { m_os.put(x,c);}
        virtual void WriteFastArrayFloat16(const Float_t*, Int_t, TStreamerElement*) {}
        virtual void WriteFastArrayDouble32(const Double_t*, Int_t, TStreamerElement*) {}
        virtual void WriteFastArray(void*, const TClass*, Int_t, TMemberStreamer*) {}
        virtual Int_t WriteFastArray(void**, const TClass*, Int_t, Bool_t, TMemberStreamer*) { return 0; }
        virtual void StreamObject(void*, const std::type_info&, const TClass*) {}
        virtual void StreamObject(void*, const char*, const TClass*) {}
        virtual void StreamObject(void*, const TClass*, const TClass*) {}
        virtual void StreamObject(TObject*) {}
        virtual void ReadBool(Bool_t&) {}
        virtual void ReadChar(Char_t&) {}
        virtual void ReadUChar(UChar_t&) {}
        virtual void ReadShort(Short_t&) {}
        virtual void ReadUShort(UShort_t&) {}
        virtual void ReadInt(Int_t&) {}
        virtual void ReadUInt(UInt_t&) {}
        virtual void ReadLong(Long_t&) {}
        virtual void ReadULong(ULong_t&) {}
        virtual void ReadLong64(Long64_t&) {}
        virtual void ReadULong64(ULong64_t&) {}
        virtual void ReadFloat(Float_t&) {}
        virtual void ReadDouble(Double_t&) {}
        virtual void ReadCharP(Char_t*) {}
        virtual void ReadTString(TString&) {}
        virtual void ReadStdString(std::string&) {}
        virtual void WriteBool(Bool_t x) { m_os << x; }
        virtual void WriteChar(Char_t x) { m_os << x; }
        virtual void WriteUChar(UChar_t x) { m_os << x; }
        virtual void WriteShort(Short_t x) { m_os << x; }
        virtual void WriteUShort(UShort_t x) { m_os << x; }
        virtual void WriteInt(Int_t x) { m_os << x; }
        virtual void WriteUInt(UInt_t x) { m_os << x; }
        virtual void WriteLong(Long_t x) { m_os << x; }
        virtual void WriteULong(ULong_t x) { m_os << x; }
        virtual void WriteLong64(Long64_t x) { m_os << static_cast<int64_t>(x); }
        virtual void WriteULong64(ULong64_t x) { m_os << static_cast<uint64_t>(x); }
        virtual void WriteFloat(Float_t x) { m_os << x; }
        virtual void WriteDouble(Double_t x ) { m_os << x; }
        virtual void WriteCharP(const Char_t* x) { std::string s(x); m_os << s; }
        virtual void WriteTString(const TString& x) { std::string s(x.Data()); m_os << s; }
        virtual void WriteStdString(const std::string &s) { m_os << s;}
        virtual TProcessID* GetLastProcessID(TRefTable*) const { return 0; }
        virtual UInt_t GetTRefExecId() { return 0; }
        virtual TProcessID* ReadProcessID(UShort_t) { return 0; }
        virtual UShort_t WriteProcessID(TProcessID*) { return 0; }
        virtual void ForceWriteInfo(TVirtualStreamerInfo*, Bool_t) {}
        virtual void ForceWriteInfoClones(TClonesArray*) {}
        virtual Int_t ReadClones(TClonesArray*, Int_t, Version_t) { return 0; }
        virtual Int_t WriteClones(TClonesArray*, Int_t) { return 0; }
        virtual Int_t ReadClassEmulated(const TClass*, void*, const TClass*) { return 0; }
        virtual Int_t ReadClassBuffer(const TClass*, void*, const TClass*) { return 0; }
        virtual Int_t ReadClassBuffer(const TClass*, void*, Int_t, UInt_t, UInt_t, const TClass*) { return 0; }
        virtual Int_t WriteClassBuffer(const TClass*, void*) { return 0; }
        virtual Int_t ReadSequence(const TStreamerInfoActions::TActionSequence&, void*) { return 0; }
        virtual Int_t ReadSequenceVecPtr(const TStreamerInfoActions::TActionSequence&, void*, void*) { return 0; }
        virtual Int_t ReadSequence(const TStreamerInfoActions::TActionSequence&, void*, void*) { return 0; }
        virtual Int_t ApplySequence(const TStreamerInfoActions::TActionSequence&, void*) { return 0; }
        virtual Int_t ApplySequenceVecPtr(const TStreamerInfoActions::TActionSequence&, void*, void*) { return 0; }
        virtual Int_t ApplySequence(const TStreamerInfoActions::TActionSequence&, void*, void*) { return 0; }
    private:
        ISostream& m_os;
    };

}

#endif // MONSVC_TISBUFFER_H_
