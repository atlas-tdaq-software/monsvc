// this is -*- c++ -*-
#ifndef MONSVC_COMMANDER_H_
#define MONSVC_COMMANDER_H_

#include <string>
#include <vector>

class TObject;
class TH1;
class TGraph;
class TGraph2D;
class TEfficiency;
class ISInfo;


namespace monsvc {
  
  class Command {
  public:
    Command() : m_cmdtype(0){}
    virtual ~Command(){}
    
    std::string m_cmd;
    unsigned int m_cmdtype;
    std::vector<std::string> m_args;
  };
  
  /**
   * @brief The base class for visitors of the Registry implenting 
   * command functionalities.
   *
   */
  class CommanderBase {
  public:
    CommanderBase(){}
    virtual ~CommanderBase(){}

    /**
     * @brief Executes command on the TObject object.
     *
     * Should be implemented by subclasses. Does nothing by default.
     */
    virtual void execute(const Command&, TObject *) {}

    /**
     * @brief Executes command on the TH1 object.
     *
     * Should be implemented by subclasses. Does nothing by default.
     */
    virtual void execute(const Command&, TH1 *) {}

    /**
     * @brief Executes command on the TGraph object.
     *
     * Should be implemented by subclasses. Does nothing by default.
     */
    virtual void execute(const Command&, TGraph *) {}

    /**
     * @brief Executes command on the TGraph2D object.
     *
     * Should be implemented by subclasses. Does nothing by default.
     */
    virtual void execute(const Command&, TGraph2D *) {}

    /**
     * @brief Executes command on the TEfficiency object.
     *
     * Should be implemented by subclasses. Does nothing by default.
     */
    virtual void execute(const Command&, TEfficiency *) {}

    /**
     * @brief Executes command on the ISInfo object.
     *
     * Should be implemented by subclasses. Does nothing by default.
     */
    virtual void execute(const Command&, ISInfo *) {}
  };
}

#endif // MONSVC_COMMANDER_H_
