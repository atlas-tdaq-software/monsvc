// -*- c++ -*-
#ifndef MONSVC_TAG_H_
#define MONSVC_TAG_H_

#include <limits>

namespace monsvc {
  
  /*
   * @brief Class holding the publication tag information.
   *
   * The handling and meaning of the tag is very different between OH and IS.
   * Therefore definining a default tag value is non-trivial. Instead, this
   * class allows knowing if a tag was provided or not and the value.
   *
   */

  class Tag {
  public:
    Tag():set(false),value(std::numeric_limits<int>::min()) {}
    explicit Tag(int t):set(true),value(t) {}
    virtual ~Tag() {};
    operator bool() const { return set; }
    bool operator ==(const Tag& other) const 
    { return (this->set==other.set) && (this->value==other.value);}
    bool set;
    int value;
  };
}

#endif  // MONSVC_TAG_H_
