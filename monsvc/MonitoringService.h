/*
 * MonitoringService.h
 *
 *  Created on: Oct 11, 2012
 *      Author: ctalau
 */

#ifndef MONSVC_MONITORING_SERVICE_H_
#define MONSVC_MONITORING_SERVICE_H_

#include "monsvc/ptr.h"
#include "monsvc/Exceptions.h"
#include "monsvc/descriptor.h"

#include <boost/noncopyable.hpp>

namespace monsvc {

    // Forward declaration of the Registry class
    namespace detail { class Registry; }

    /*
     * @brief The MonitoringService is the interface through which objects to be
     * published can be registered.
     *
     * A callback method can be registered. It is called just before the object
     * is published.
     *
     * The 'own' flag indicates if the MonitoringService should take ownership
     * of the object.
     *
     * A ptr<T> object is returned that can be used to access
     * the object in a thread-safe way with regard to the publishing
     * service. See ptr.h
     *
     * A object can be found by name and returned as ptr<T> by the find_object()
     * method. The type T has to be the same or a base type of the original
     * class.
     *
     * Objects can be removed from registration by name or via the ptr<T> handle.
     *
     * All operations on the MonitoringService are thread-safe.
     *
     * The MonitoringService is a singleton that can only be accessed
     * via the static instance() method.
     */
    class MonitoringService : boost::noncopyable {
    public:
        /**
         * @brief Return the MonitoringServie singleton.
         */
        static MonitoringService& instance();

        /**
         * @brief Registers an object to be monitored.
         *
         * The monitoring service takes a copy of provided pointer and stores
         * it into an internal data structure. The caller should use the 
         * returned @ref ptr to access the object.
         *
         * In order for the object to be published, the application should 
         * configure the monitoring service by using the 
         * OnlineMonitoringConfiguration or configure several publishers by
         * hand.
         *
         * @param obj_name Object name, will be used to compose the publication
         * name
         * @param obj Object pointer
         * @param own If true, the MonitoringService will take ownership of the
         * object (i.e. it will delete it).
         * @param precb Callback function that will be called just before the 
         * object publication. Type must be void ()(const std::string&, T*)
         * @param postcb Callback function that will be called just after the 
         * object publication. Type must be void ()(const std::string&, T*)
         *
         * @throws NameExists if another object with same name exists.
         */
        template <typename T, typename F = std::function<void (const std::string&, T*)>, typename P = std::function<void (const std::string&, T*)> >
          ptr<T> register_object(const std::string& obj_name, T * obj, 
                                 bool own = false,
                                 F precb = 0, P postcb = 0);

        /**
         * @brief Registers an object to be monitored.
         *
         * The monitoring service takes a copy of provided pointer and stores
         * it into an internal data structure. The caller should use the 
         * returned @ref ptr to access the object.
         *
         * In order for the object to be published, the application should 
         * configure the monitoring service by using the 
         * OnlineMonitoringConfiguration or configure several publishers by
         * hand.
         *
         * @param obj_name Object name, will be used to compose the publication
         * name
         * @param tag Tag value for publishers supporting tagging. Multiple
         * objects with the same name can registered, as long as the tag is
         * different.
         * @param obj Object pointer
         * @param own If true, the MonitoringService will take ownership of the
         * object (i.e. it will delete it).
         * @param precb Callback function that will be called just before the 
         * object publication. Type must be void ()(const std::string&, T*)
         * @param postcb Callback function that will be called just after the 
         * object publication. Type must be void ()(const std::string&, T*)
         *
         * @throws NameExists if another object with same name and tag exists.
         */
        template <typename T, typename F = std::function<void (const std::string&, T*)>, typename P = std::function<void (const std::string&, T*)> >
          ptr<T> register_object(const std::string& obj_name, int tag,
                                 T * obj,
                                 bool own = false,
                                 F precb = 0, P postcb = 0);
        
        /**
         * @brief Registers an object to be monitored.
         *
         * The monitoring service takes a copy of provided pointer and stores
         * it into an internal data structure. The caller should use the 
         * returned @ref ptr to access the object.
         *
         * In order for the object to be published, the application should 
         * configure the monitoring service by using the 
         * OnlineMonitoringConfiguration or configure several publishers by
         * hand.
         *
         * @param obj_name Object name, will be used to compose the publication
         * name
         * @param tag Tag value for publishers supporting tagging. Multiple
         * objects with the same name can registered, as long as the tag is
         * different.
         * @param obj Object pointer
         * @param own If true, the MonitoringService will take ownership of the
         * object (i.e. it will delete it).
         * @param precb Callback function that will be called just before the 
         * object publication. Type must be void ()(const std::string&, T*)
         * @param postcb Callback function that will be called just after the 
         * object publication. Type must be void ()(const std::string&, T*)
         *
         * @throws NameExists if another object with same name and tag exists.
         */
        template <typename T, typename F = std::function<void (const std::string&, T*)>, typename P = std::function<void (const std::string&, T*)> >
          ptr<T> register_object(const std::string& obj_name, const Tag& tag,
                                 T * obj, bool own = false,
                                 F precb = 0, P postcb = 0);

        /**
         * @brief Registers an object to be monitored.
         *
         * The monitoring service takes a copy of provided pointer and stores
         * it into an internal data structure. The caller should use the 
         * returned @ref ptr to access the object.
         *
         * In order for the object to be published, the application should 
         * configure the monitoring service by using the 
         * OnlineMonitoringConfiguration or configure several publishers by
         * hand.
         *
         * @param obj_name Object name, will be used to compose the publication
         * name
         * @param annotations A list of annotations. It will be added to
         * publications for the publisher supporting them.
         * @param obj Object pointer
         * @param own If true, the MonitoringService will take ownership of the
         * object (i.e. it will delete it).
         * @param precb Callback function that will be called just before the 
         * object publication. Type must be void ()(const std::string&, T*)
         * @param postcb Callback function that will be called just after the 
         * object publication. Type must be void ()(const std::string&, T*)
         *
         * @throws NameExists if another object with same name and tag exists.
         */
        template <typename T, typename F = std::function<void (const std::string&, T*)>, typename P = std::function<void (const std::string&, T*)> >
          ptr<T> register_object(const std::string& obj_name,
                                 const Annotations& annotations,
                                 T * obj, bool own = false,
                                 F precb = 0, P postcb = 0);

        /**
         * @brief Registers an object to be monitored.
         *
         * The monitoring service takes a copy of provided pointer and stores
         * it into an internal data structure. The caller should use the 
         * returned @ref ptr to access the object.
         *
         * In order for the object to be published, the application should 
         * configure the monitoring service by using the 
         * OnlineMonitoringConfiguration or configure several publishers by
         * hand.
         *
         * @param obj_name Object name, will be used to compose the publication
         * name
         * @param tag Tag value for publishers supporting tagging. Multiple
         * objects with the same name can registered, as long as the tag is
         * different.
         * @param annotations A list of annotations. It will be added to
         * publications for the publisher supporting them.
         * @param obj Object pointer
         * @param own If true, the MonitoringService will take ownership of the
         * object (i.e. it will delete it).
         * @param precb Callback function that will be called just before the 
         * object publication. Type must be void ()(const std::string&, T*)
         * @param postcb Callback function that will be called just after the 
         * object publication. Type must be void ()(const std::string&, T*)
         *
         * @throws NameExists if another object with same name and tag exists.
         */
        template <typename T, typename F = std::function<void (const std::string&, T*)>, typename P = std::function<void (const std::string&, T*)> >
          ptr<T> register_object(const std::string& obj_name, const Tag& tag,
                                 const Annotations& annotations,
                                 T * obj, bool own = false,
                                 F precb = 0, P postcb = 0);

        /**
         * @brief Find an object with the given @c name and no tag.
         *
         * The template argument should be the @b exact type of the object to
         * be returned. A base class is not sufficient.
         *
         * @throws WrongType if retrieved object is not convertible to @c T.
         * @throws NotFound if there is no object with the given @c name.
        */
        template <typename T>
          ptr<T> find_object(const std::string& name) const;
          
        /**
         * @brief Find an object with the given @c name and tag.
         *
         * The template argument should be the @b exact type of the object to
         * be returned. A base class is not sufficient.
         *
         * @throws WrongType if retrieved object is not convertible to @c T.
         * @throws NotFound if there is no object with the given @c name.
        */
        template <typename T>
          ptr<T> find_object(const std::string& name, int tag) const;

        /**
         * @brief Stop monitoring the object with the specified name.
         */
        bool remove_object(const std::string& name);
        
        /**
         * @brief Stop monitoring the object with the specified name.
         */
        bool remove_object(const char* name);

        /**
         * @brief Stop monitoring the object with the specified name and tag.
         */
        bool remove_object(const std::string& name, int tag);
        
        /**
         * @brief Stop monitoring the object with the specified name and tag.
         */
        bool remove_object(const char* name, int tag);

        /**
         * @brief Stop monitoring all objects with the specified name.
         */
        int remove_objects(const std::string& name);

        /**
         * @brief Stop monitoring all objects with the specified name.
         */
        int remove_objects(const char* name);
        
        /**
         * @brief Stop monitoring the specific object.
         *
         * The parameter @c obj should have the @b exact type of the object
         * to be deleted. A base class is not sufficient.
         */
        template<typename T>
        bool remove_object(T *obj);

    private:
        typedef std::shared_ptr<descriptor_base> DescriptorPtr;

        MonitoringService();
        void publish_object(const std::string& name, DescriptorPtr rep);
        DescriptorPtr 
          find_object(const std::string& name, const Tag& tag) const;
        bool remove_if(std::function<bool(DescriptorPtr)> f);
        template <typename T> static bool is_same(T* obj, DescriptorPtr desc);

        detail::Registry& m_registry;
    };

    template <typename T, typename F, typename P>
      ptr<T> MonitoringService::register_object(const std::string& obj_name,
                                                const Tag& tag,
                                                const Annotations& ann,
                                                T* obj, bool own,
                                                F precb, P postcb)
    {
        std::shared_ptr<descriptor<T>> 
            rep(std::make_shared<descriptor<T>>(obj_name, tag, ann, obj, own, 
                                                precb, postcb));
        
        publish_object(obj_name, rep);
        ptr<T> result(rep);
        return result;
    }

    template <typename T, typename F, typename P>
      ptr<T> MonitoringService::register_object(const std::string& obj_name,
                                                const Tag& tag,
                                                T* obj, bool own,
                                                F precb, P postcb)
    {
        Annotations a;
        return register_object(obj_name, tag, a, obj, own, precb, postcb);
    }

    template <typename T, typename F, typename P>
      ptr<T> MonitoringService::register_object(const std::string& obj_name,
                                                const Annotations& ann,
                                                T* obj, bool own,
                                                F precb, P postcb)
    {
        Tag t;
        return register_object(obj_name, t, ann, obj, own, precb, postcb);
    }

    template <typename T, typename F, typename P>
      ptr<T> MonitoringService::register_object(const std::string& obj_name,
                                                int tag,
                                                T* obj, bool own,
                                                F precb, P postcb)
    {
        Tag t(tag);
        return register_object(obj_name, t, obj, own, precb, postcb);
    }

    template <typename T, typename F, typename P>
      ptr<T> MonitoringService::register_object(const std::string& obj_name, 
                                                T* obj, bool own,
                                                F precb, P postcb)
    {
        Tag t;
        return register_object(obj_name, t, obj, own, precb, postcb);
    }

    template <typename T>
      ptr<T> MonitoringService::find_object(const std::string& name) const
    {
      Tag t;
      DescriptorPtr desc = find_object(name, t);
      if(descriptor<T> *p = dynamic_cast<descriptor<T>*>(desc.get())) {
        std::shared_ptr<descriptor<T> > r(desc, p);
        return ptr<T>(r);
      } else {
        throw WrongType(ERS_HERE, name);
      }
    }

    template <typename T>
      ptr<T> MonitoringService::find_object(const std::string& name, int tag) const
    {
      Tag t(tag);
      DescriptorPtr desc = find_object(name, t);
      if(descriptor<T> *p = dynamic_cast<descriptor<T>*>(desc.get())) {
        std::shared_ptr<descriptor<T> > r(desc, p);
        return ptr<T>(r);
      } else {
        throw WrongType(ERS_HERE, name);
      }
    }

    template <typename T>
      bool MonitoringService::is_same(T* obj, DescriptorPtr desc)
    {
       if(descriptor<T> *p = dynamic_cast<descriptor<T>*>(desc.get())) {
           // correct type, now check identity
           if (p->object() == obj) {
               return true;
           }
       }
       return false;
    }

    template<class T>
      bool MonitoringService::remove_object(T *obj)
    {
        bool (&fun)(T*, DescriptorPtr)= is_same;
        return remove_if(std::bind(fun, obj, std::placeholders::_1));
    }

} /* namespace monsvc */
#endif /* MONSVC_MONITORING_SERVICE_H_ */

/* Local Variables: */
/* c-basic-offset:4 */
/* End: */
